//
// VS_HudDeath.sma
// 观察者模式显示时,显示当前玩家数据
// http:cs27015.com
//
#include <amxmodx>
#include <csstats> 
#include "li_geoip"

#include "VGuard2022/VGStats.inc"
#include "VGuard2022/VSLogin.inc"

#define PLUGIN 	"VSHudDeath"
#define VERSION "3.01"
#define AUTHOR 	"Koma"

#define ID_TASK_GET		3300
#define is_player(%1) 	(1<=%1<=32)

#define AD_QQ_ADMIN		"277460272"

// 调试模式
#define DEBUG			0

new g_SpecMode[33]
new const member[][] 	= {"BOT", "未注册", "会员"}

// 当前武器ID
new g_PlayerWeapon[33]

// 武器名称
new const weapon_name[33][] = {
	"NULL",
	"p228",
	"glock",
	"scout",
	"hegrendade",
	"xm1014",
	
	"C4",
	"mac10",
	"aug",
	"smoke",
	"elite",
	
	"fiveseven",
	"ump45",
	"sg550",
	"gali",
	"famas",
	
	"usp",
	"glock18",
	"awp",
	"mp5",
	"m249",
	
	"m3",
	"m4a1",
	"tmp",
	"g3sg1",
	"flashbang",
	
	"deagle",
	"sg552",
	"ak47",
	"knife",
	"p90",
	"vest",
	"vesthelm"
};

public plugin_init()
{
	register_plugin( PLUGIN, VERSION, AUTHOR);
	register_event( "StatusValue", "DisplayInfo", "bd", "1=2");
	register_event( "TextMsg", "SetSpecMode", "bd", "2&ec_Mod");
	register_event( "CurWeapon", "_CurWeapon", "be", "1=1");
} 

// 获取玩家当前武器
public _CurWeapon(id)
{
	if(!is_player(id) || is_user_bot(id) || !is_user_connected(id))
		return PLUGIN_CONTINUE;
	
	new weapon = read_data(2)
	g_PlayerWeapon[id] = weapon
	return PLUGIN_CONTINUE;
}

public SetSpecMode(id){	
	new arg[12];
	read_data( 2, arg, 11);
	g_SpecMode[id] = ( arg[10] == '4' );
}

// MTLogin登录成功事件
public client_login(id, uid, exp, type)
{
	static param[3]
	param[0] = id
	param[1] = get_user_userid(id)
	param[2] = uid
}

public DisplayInfo(id){

	if ( g_SpecMode[id])
	{
		new player = read_data(2);
		new szMessage[256];
		new szName[32];
		new szIP[16], szCountry[40], szArea[64];
		new szSignature[128];
		static user_type

		get_user_name( player, szName, 31);
		get_user_ip( player, szIP, 15, 1);
		get_ipinfo( szIP, szCountry, szArea, 39, 63);

		user_type = get_user_type(player)
		get_user_signature(player,szSignature,128)
	
		if ( is_user_connected( player))
		{
			new iRankPos;
			new iRankMax;
			new kills,deaths,head;
			new wid, wkill, whead, wshot, whit

			if(user_type <= MT_UNREG)
			{
				format( szMessage, 255, "【昵称】%s^n【权限】%s^n【排名】0/%d^n【战况】杀敌:0  阵亡:0 爆头率:0.00^% K/D:0.00^n【来自】%s - %s^n【签名】申请赞助请联系QQ%s",
					szName,
					member[user_type],
					iRankMax,
					szCountry,
					szArea,
					AD_QQ_ADMIN);
			}
			else
			{
				wid = g_PlayerWeapon[player]
				if(wid >= 33)
					wid = 0

				iRankPos = vs_get_rank(player);
				iRankMax = vs_get_maxplayer();
				
				// 武器数据
				wkill = vs_get_rank_data(player, wid, VS_KILL);
				whead = vs_get_rank_data(player, wid, VS_HEADSHOT);
				wshot = vs_get_rank_data(player, wid, VS_SHOT);
				whit = vs_get_rank_data(player, wid, VS_HIT);
				
				if(wkill == 0)
					wkill = 1;
				if(wshot == 0)
					wshot = 1;
				
				kills  = vs_get_kill(player)
				if(kills == 0)
					kills = 1;
				
				deaths = vs_get_death(player)
				if(deaths == 0)
					deaths = 1;
				
				head   = vs_get_head(player)
				if(szSignature[0] == 0)
					format( szSignature, 127, "申请赞助请联系QQ", AD_QQ_ADMIN)

				format( szMessage, 255, "【昵称】%s^n【权限】%s^n【排名】%d/%d^n【总榜】杀敌:%d,阵亡:%d,爆头率:%0.2f,K/D:%0.1f^n【武器】(%s)命中率:%0.1f,爆头率:%0.1f^n【来自】%s - %s^n【签名】%s",
					szName,
					member[user_type],
					
					iRankPos,
					iRankMax,

					kills,
					deaths,
					100.0 * float(head)/float(kills),
					float(kills)/float(deaths),
					
					weapon_name[wid],
					100.0 * float(whit)/float(wshot),
					100.0 * float(whead)/float(wkill),

					szCountry,
					szArea,
					szSignature);
			}
			
			// 不同权限文字颜色不同
			static red, green, blue
			switch(user_type)
			{
				case MT_NONSTEAM:
				{
					red = 0
					green = 255
					blue = 0
				}
				default:
				{
					red = 255
					green = 255
					blue = 255
				}
			}

			set_hudmessage(red, green, blue, 0.45, 0.60, 0, 0.0, 10.0, 0.0, 0.0, 2)				
			show_hudmessage(id, szMessage)
		}
	}
}

get_ipinfo( szIP[], szCountry[], szArea[], clen, alen){
	
	//可能 li_geoip 模块将 192.168.*.* 网段固定为"内网用户", 这里提供修改的办法.
	if( strfind( szIP, "192.168.") == 1){
		
		/*修改成你自己的网吧的地区名*/
		copy( szCountry, clen, "***本地***");
		
		/*修改成你自己的网吧的名称*/
		copy( szArea, alen, "***本机***");
	}
	else
		geoip_info( szIP, szCountry, szArea, clen, alen);
}
