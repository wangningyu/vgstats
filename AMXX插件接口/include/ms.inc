#if defined _ms_included
  #endinput
#endif
#define _ms_included

#pragma library ms

#define MS_VERSION                               "4.4b"
#define MS_AUTHOR                                "idlecode@qq.com"

#define MAX_LEVEL                                (30+1)
#define MAX_WEAPON                               (32+1)
#define MAX_PLAYER                               (32+1)

#define MAX_IP_LENGTH                            15
#define MAX_MD5_LENGTH                           34
#define MAX_NAME_LENGTH                          31
#define MAX_SIGNATURE_LENGTH                     63

// Player Data Set
#define MS_DAILY                                 0
#define MS_WEEKLY                                1
#define MS_MONTHLY                               2
#define MS_QUARTERLY                             3
#define MS_YEARLY                                4
#define MS_ALL                                   5
#define MS_MAP                                   6

// Player Daily State Parameter

// Player Bomb State Parameter
#define BOMB_PLANTING                            0
#define BOMB_PLANTED                             1
#define BOMB_EXPLODE                             2
#define BOMB_DEFUSING                            3
#define BOMB_DEFUSED                             4
#define BOMB_INDEX                               5

// Player Round State Parameter
#define ROUND_T                                  0
#define ROUND_CT                                 1
#define ROUND_S                                  2
#define ROUND_WINT                               3
#define ROUND_WINCT                              4
#define ROUND_FIRSTKILL                          5
#define ROUND_FIRSTDEATH                         6
#define ROUND_LASTKILL                           7
#define ROUND_LASTDEATH                          8
#define ROUND_INDEX                              9

// Player Weapon State Parameter
#define WEAPON_KILL                              0
#define WEAPON_DEATH                             1
#define WEAPON_HEADSHOT                          2
#define WEAPON_KILLED                            3
#define WEAPON_HEADSHOTED                        4
#define WEAPON_TEAMKILL                          5
#define WEAPON_SHOOT                             6
#define WEAPON_HIT                               7
#define WEAPON_DAMAGE                            8
#define WEAPON_INDEX                             9

// Player BodyPart State Parameter
#define BODYPART_WHOLE                           0
#define BODYPART_HEAD                            1
#define BODYPART_CHEST                           2
#define BODYPART_STOMACH                         3
#define BODYPART_LEFTARM                         4
#define BODYPART_RIGHTARM                        5
#define BODYPART_LEFTLEG                         6
#define BODYPART_RIGHTLEG                        7
#define BODYPART_UNKNOWN                         8
#define BODYPART_INDEX                           9

//*********************************     My Stats Score     ***********************************此模块必须加载,附源码,可自定义
//得分计算公式
native Float:ms_calc_score(explode,defused,win,firstkill,firstdeath,lastkill,lastdeath,kill,death,headshot,headshoted,teamkill,onlinetime);

//*********************************        My Stats        ***********************************此模块必须加载
//设置玩家在数据库中的uid,设置0表示未注册玩家,设置>0表示注册玩家,设置<0表示密码错误,会被kick
native ms_set_user_uid(id,uid);//该函数主要在My Stats Login中进行调用

//获取玩家在数据库中的uid
native ms_get_user_uid(id);

//获取数据库中的玩家数量-快速(im可以是MS_MAP或其他)
native ms_get_user_count0(im);

//获取数据库中的玩家数量-准确(im可以是MS_MAP或其他)
native ms_get_user_count1(im);

//获取玩家排名(im可以是MS_DAILY 或 MS_WEEKLY 或 MS_MONTHLY 或 MS_QUARTERLY 或 MS_YEARLY 或 MS_ALL 或 MS_MAP，下同)
native ms_get_user_rank(im,id);

//获取玩家在线时间
native ms_get_user_onlinetime(im,id);

//获取玩家得分
native Float:ms_get_user_score(im,id);

//获取玩家安包数据
native ms_get_user_bombstats(im,id,ibomb[BOMB_INDEX]);

//获取玩家回合数据
native ms_get_user_roundstats(im,id,iround[ROUND_INDEX]);

//获取玩家武器数据,如果iwn为0,则获取所有武器的数据
native ms_get_user_weaponstats(im,id,iwn,iweapon[WEAPON_INDEX]);

//获取玩家命中数据,如果iwn为0,则获取所有武器的数据
native ms_get_user_bodypartstats(im,id,iwn,ibodypart[BODYPART_INDEX]);

//*********************************     My Stats Build     ***********************************第一次加载并完成数据库创建后可以不再加载此模块
//NULL

//*********************************     My Stats Login     ***********************************此模块必须加载,论坛版附源码,可自定义
//玩家登录
native ms_client_login(id);

//获取玩家签名
native ms_get_user_signature(id,cSignature[MAX_SIGNATURE_LENGTH+1]);

//*********************************     My Stats Level     ***********************************此模块推荐加载
//获取玩家等级,如果nameflag为1,则设置等级名称到cLName
native ms_get_user_level(id,nameflag,cLName[]);

//获取分数对应的等级,返回值:等级;如果nameflag为1,则设置等级名称到cLName
native ms_get_level(Float:fScore,nameflag,cLName[]);

//获取等级对应的名字
native ms_get_level_name(iLevel,cName[]);

//获取等级对应的分数
native Float:ms_get_level_score(iLevel);

//*********************************   My Stats Spectator   ***********************************此模块推荐加载
//获取玩家正在观察的对方玩家id
native ms_get_user_spectator(id);

//*********************************     My Stats Other     ***********************************此模块推荐加载
//显示玩家的进服提示信息
native ms_fwd_user_enter(id);

//显示玩家的离服提示信息
native ms_fwd_user_leave(id);
