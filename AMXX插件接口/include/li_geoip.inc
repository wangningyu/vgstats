/* Li_GeoIP module functions for AMX Mod X
   by Lwj "Icarusli" GDMM
   (C)Copyrighted under the GNU General Public License, Version 2.0
 */

#if defined _li_geoip_included
  #endinput
#endif
#define _li_geoip_included


#if AMXX_VERSION_NUM >= 175
 #pragma reqlib li_geoip
 #if !defined AMXMODX_NOAUTOLOAD
  #pragma loadlib li_geoip
 #endif
#else
 #pragma library li_geoip
#endif


/*Example:
 *new country[40]
 *new area[64]
 *new ip[16]
 *geoip_info(ip, country, area, 39, 63)
 */
//
native geoip_info(ip[], country[], area[], country_len=39 ,area_len=135);
