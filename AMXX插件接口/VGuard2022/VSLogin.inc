#if defined _mt_login_included
    #endinput
#endif

#define _mt_login_included

#pragma reqlib "mt_login"

////////////////////////////////////////////////////////////
//
// VIP类型
//
enum
{
	MT_UNKNOW = 0,			// 未检查
	MT_UNREG,				// 未注册
	MT_NONSTEAM,			// 会员
}

////////////////////////////////////////////////////////////
//
// 玩家数据库资料
//

// 获取玩家数字编号
native get_user_uid(id)

// 获取玩家VIP类型
native get_user_type(id)

// 获取玩家英文名
native get_user_engname(id, engname[], len)

// 获取玩家中文名
native get_user_chsname(id, chsname[], len)

// 获取玩家qq
native get_user_qq(id, qq[], len)

// 获取玩家签名
native get_user_signature(id, signature[], len)

// 获取玩家权限字母a-z
native get_vip_flag(id, flag[], len)

// 获取玩家权限到期时间
native get_exp_date(id, expdate[], len)

// 获取玩家注册时间
native get_reg_date(id, regdate[], len)



////////////////////////////////////////////////////////////
//
// MySQL数据库 (以便别的插件调用)
//

// 获取数据库地址
native get_sql_host(hst[], len)

// 获取数据库用户名
native get_sql_user(usr[], len)

// 获取数据库密码
native get_sql_pass(pwd[], len)

// 获取数据库名
native get_sql_db(dbname[], len)

// 添加管理日志
native log_admin_mysql(string[])


////////////////////////////////////////////////////////////
//
// 回调函数 (当玩家登录或注册成功时会触发)
//

// 玩家登录事件
forward client_login(id, uid, exp, type)

/* AMXX-Studio Notes - DO NOT MODIFY BELOW HERE
*{\\ rtf1\\ ansi\\ ansicpg936\\ deff0{\\ fonttbl{\\ f0\\ fnil\\ fcharset134 Tahoma;}}\n\\ viewkind4\\ uc1\\ pard\\ lang2052\\ f0\\ fs16 \n\\ par }
*/
