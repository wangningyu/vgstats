#pragma reqlib vgstats

#define CSW_NONE            0

/*
// 特别说明：CSW_NONE代表总榜数据
// amxconst.inc
#define CSW_P228            1
#define CSW_GLOCK           2  // Unused by game, See CSW_GLOCK18.
#define CSW_SCOUT           3
#define CSW_HEGRENADE       4
#define CSW_XM1014          5
#define CSW_C4              6
#define CSW_MAC10           7
#define CSW_AUG             8
#define CSW_SMOKEGRENADE    9
#define CSW_ELITE           10
#define CSW_FIVESEVEN       11
#define CSW_UMP45           12
#define CSW_SG550           13
#define CSW_GALI            14
#define CSW_GALIL           14
#define CSW_FAMAS           15
#define CSW_USP             16
#define CSW_GLOCK18         17
#define CSW_AWP             18
#define CSW_MP5NAVY         19
#define CSW_M249            20
#define CSW_M3              21
#define CSW_M4A1            22
#define CSW_TMP             23
#define CSW_G3SG1           24
#define CSW_FLASHBANG       25
#define CSW_DEAGLE          26
#define CSW_SG552           27
#define CSW_AK47            28
#define CSW_KNIFE           29
#define CSW_P90             30
#define CSW_VEST            31  // Custom
#define CSW_VESTHELM        32  // Custom
*/

//////////////////////////////////////////////////////////////////////////
// 全部排名数据 (此类型与武器、总榜有关，与用户资料无关)
//////////////////////////////////////////////////////////////////////////
enum VS_INFO
{
	VS_KILL,			// 杀敌
	VS_SHOT,			// 射击
	VS_HEADSHOT,		// 爆头
	VS_HIT,				// 击中

	VS_DAMAGE,			// 伤害
	VS_DEATH,			// 死亡
	VS_FIRSTKILL,		// 首杀
	VS_FIRSTDEATH,		// 首死
	VS_BOMB_DEFUSION,	// 拆除C4

	VS_BOMB_PLANTING,	// 安装C4
	VS_TIME_ONLINE,		// 在线(每秒+0.002分,每小时7.2分)

	VS_KILL_WORLD,		// 摔死
	VS_KILL_SELF,		// 自杀次数

	VS_MAX_PLAYER,		// 最大玩家
	VS_RANK,			// 当前排名
	VS_SCORE,			// 当前积分

	// 身体伤害
	VS_DMAGE_NONE,		// 击中空枪
	VS_DMAGE_HEAD,		// 击中头部
	VS_DMAGE_CHEST,		// 击中胸部
	VS_DMAGE_STOMACH,	// 击中胃部
	VS_DMAGE_LEFTARM,	// 击中左臂
	VS_DMAGE_RIGHTARM,	// 击中右臂
	VS_DMAGE_LEFTEG,	// 击中左脚
	VS_DMAGE_RIGHTEG,	// 击中右脚
	VS_DMAGE_SHIELD,	// 击中盾牌

	// 武器+BKILL
	VS_BKILL,			// 被击杀
	VS_BHEAD,			// 被爆头
	
	// 击中次数
	VS_HIT_NONE,		// 击中空枪
	VS_HIT_HEAD,		// 击中头部
	VS_HIT_CHEST,		// 击中胸部
	VS_HIT_STOMACH,		// 击中胃部
	VS_HIT_LEFTARM,		// 击中左臂
	VS_HIT_RIGHTARM,	// 击中右臂
	VS_HIT_LEFTEG,		// 击中左脚
	VS_HIT_RIGHTEG,		// 击中右脚
	VS_HIT_SHIELD,		// 击中盾牌

	// 混战参数 add by MT 2023-09-30
	VS_ROUND,			// 总回合
	VS_RWIN_T,			// 回合：T杀完胜利
	VS_RWIN_BOOM,		// 回合：T爆炸胜利
	VS_RWIN_CT,			// 回合：CT杀完胜利
	VS_RWIN_DEFUSE,		// 回合：CT爆炸胜利
	VS_RWIN_SAVED,		// 回合：CT时间结束胜利
	VS_RWIN_RESCUE,		// 回合：CT解救人质胜利
	VS_RWIN_NOT_RESCUE,	// 回合：CT未解救人质胜利
	VS_RWIN_TYPE1,		// 回合：保留胜利1
	VS_RWIN_TYPE2,		// 回合：保留胜利2

	VS_RLOSE,			// 失败回合
	VS_REVEN,			// 平局回合

	// 比赛参数
	VS_SESSION,			// 总场次
	VS_SWIN,			// 胜利场次
	VS_SLOSE,			// 失败场次
	VS_SEVEN,			// 平局场次

	VS_MVP,				// 最佳次数
	VS_RWS,				// 每局贡献评分(伤害占比)

	// 每回合杀敌统计
	VS_KILL_0,			// 0K 酱油局
	VS_KILL_1,			// 1K
	VS_KILL_2,			// 2K
	VS_KILL_3,			// 3K
	VS_KILL_4,			// 3K
	VS_KILL_5,			// 5K
	VS_KILL_6,			// 6K
	VS_KILL_7,			// 7K
	VS_KILL_8,			// 8K
	VS_KILL_9,			// 9K
	VS_KILL_10,			// 10K
	VS_KILL_11,			// 11K
	VS_KILL_12,			// 12K
	VS_KILL_13,			// 13K
	VS_KILL_14,			// 14K
	VS_KILL_15,			// 15K
	VS_KILL_16,			// 16K

	// 残局统计
	VS_1V1,				// 1v1
	VS_1V2,				// 1v2
	VS_1V3,				// 1v3
	VS_1V4,				// 1v4
	VS_1V5,				// 1v5
	VS_1V6,				// 1v6
	VS_1V7,				// 1v7
	VS_1V8,				// 1v8
	VS_1V9,				// 1v9
	VS_1V10,			// 1v10
	VS_1V11,			// 1v11
	VS_1V12,			// 1v12
	VS_1V13,			// 1v13
	VS_1V14,			// 1v14
	VS_1V15,			// 1v15
	VS_1V16,			// 1v16

	// 残局信息
	VS_1ROUND,			// 残局场次
	VS_1RWIN,			// 残局胜利

	VS_ASSIST,			// 助攻次数
	VS_ADR,				// 场均实际伤害占比累计
	
	// 穿墙信息
	VS_WALL_HIT,		// 穿墙累计命中次数
	VS_WALL_DAMAGE,		// 穿墙累计射击伤害
	VS_WALL_HEAD,		// 穿墙累计爆头次数
	VS_WALL_KILL,		// 穿墙累计击杀次数

	VS_BWALL_HIT,		// 被穿墙累计命中次数
	VS_BWALL_DAMAGE,	// 被穿墙累计射击伤害
	VS_BWALL_HEAD,		// 被穿墙累计爆头次数
	VS_BWALL_KILL,		// 被穿墙累计击杀次数
}

// 申请读取玩家排名基本信息,参见MTRedis.inc
native vs_load_rank(id, uid);

// 获取全部信息 参见VS_INFO
native vs_get_stats(index, stats[32]);

// 首次加载武器数据(只需要调用一次即可)
native vs_load_all_weapon_data(id, uid);

// 获取游戏数据
// 参数:
// id:		游戏序号(1-32)
// uid:		数据库编号(大于0)
// nWeapon:	武器编号(CSW_NONE代表总榜, 范围0-32)
// nType:	数据类型
//
// 读取个人总榜示例：
// 读取总人数：			new val = vs_get_rank_data(id, CSW_NONE, VS_MAX_PLAYER);
// 读取排名：			new val = vs_get_rank_data(id, CSW_NONE, VS_RANK);
// 读取总杀敌：			new val = vs_get_rank_data(id, CSW_NONE, VS_KILL);
// 读取总死亡：			new val = vs_get_rank_data(id, CSW_NONE, VS_DEATH);
// 读取总射击：			new val = vs_get_rank_data(id, CSW_NONE, VS_SHOT);
// 读取总命中：			new val = vs_get_rank_data(id, CSW_NONE, VS_HIT);
// 读取总爆头次数：		new val = vs_get_rank_data(id, CSW_NONE, VS_HEADSHOT);
// 读取总穿墙命中次数：	new val = vs_get_rank_data(id, CSW_NONE, VS_WALL_HIT);
// 读取总穿墙爆头次数：	new val = vs_get_rank_data(id, CSW_NONE, VS_WALL_HEAD);
// 读取总穿墙击杀次数：	new val = vs_get_rank_data(id, CSW_NONE, VS_WALL_KILL);
// 读取总场次：			new val = vs_get_rank_data(id, CSW_NONE, VS_ROUND);
// 读取T杀完胜利：		new val = vs_get_rank_data(id, CSW_NONE, VS_RWIN_T);
// 读取T爆炸胜利：		new val = vs_get_rank_data(id, CSW_NONE, VS_RWIN_BOOM);
// 读取残局场次：		new val = vs_get_rank_data(id, CSW_NONE, VS_1ROUND);
// 读取残局胜利场次：	new val = vs_get_rank_data(id, CSW_NONE, VS_1RWIN);
// 读取残1v1胜利场次：	new val = vs_get_rank_data(id, CSW_NONE, VS_1V1);
// 读取残回合1杀场次：	new val = vs_get_rank_data(id, CSW_NONE, VS_KILL_1);
//
// 
// 读取个人武器数据示例：
// 读取AK射击次数：		new val = vs_get_rank_data(id, CSW_AK47, VS_SHOT);
// 读取AK命中总次数：	new val = vs_get_rank_data(id, CSW_AK47, VS_HIT);
// 读取AK爆头总次数：	new val = vs_get_rank_data(id, CSW_AK47, VS_HEADSHOT);
// 读取AK击中头总次数：	new val = vs_get_rank_data(id, CSW_AK47, VS_HIT_HEAD);
// 读取AK击胸部总次数：	new val = vs_get_rank_data(id, CSW_AK47, VS_HIT_CHEST);
// 所有击中胸部总次数：	new val = vs_get_rank_data(id, CSW_NONE, VS_HIT_CHEST);

// 读取AK穿墙命中次数：	new val = vs_get_rank_data(id, CSW_AK47, VS_WALL_HIT);
// 读取AK穿墙爆头次数：	new val = vs_get_rank_data(id, CSW_AK47, VS_WALL_HEAD);
// 读取AK穿墙击杀次数：	new val = vs_get_rank_data(id, CSW_AK47, VS_WALL_KILL);
// 读取AK总伤害：		new val = vs_get_rank_data(id, CSW_AK47, VS_DAMAGE);
native vs_get_rank_data(id, nWeapon, nType);


// 获取当前数字编号(由vs_load_rank传入的)
native vs_get_uid(id);

// 获取玩家当前排名
native vs_get_rank(id);

// 获取总玩家数量
native vs_get_maxplayer();

// 获取玩家经验值
native vs_get_exp(id);

// 获取玩家总杀敌
native vs_get_kill(id);

// 获取玩家总射击次数
native vs_get_shot(id);

// 获取玩家总爆头次数
native vs_get_head(id);

// 获取玩家总命中次数
native vs_get_hit(id);

// 获取玩家总伤害值
native vs_get_damage(id);

// 获取玩家总死亡
native vs_get_death(id);

// 获取玩家率先杀敌
native vs_get_fkill(id);

// 获取玩家率先阵亡
native vs_get_fdeath(id);

// 获取玩家拆除C4次数
native vs_get_defu(id);

// 获取玩家安装C4次数
native vs_get_plant(id);

// 获取在线时长 (返回秒值)
native vs_get_online(id);

// 获取在线时间 (字符串)
// 示例:
//  new szHour[128]
//  vs_get_online_hour(id, szHour, 128)
//  szHour = "123.45"
native vs_get_online_hour(id, szText[], len);


// 设置是否开启排名统计
// bUsed	是否启用
native vs_set_enable(bUsed);

// 获取当前是否开启排名统计
native vs_get_enable();


//////////////////////////////////////////////////////////////////////////
// 玩家用户表的数据类型 (如果PHP中未使用则插件不需要调用)
// 此数据与武器或排名均无任何关系，仅与用户资料无关
//////////////////////////////////////////////////////////////////////////
enum USER_INFO
{
	ECT_SET_NAMEEN = 0,	// 设置英文名
	ECT_SET_NAMECH,		// 设置中文名
	ECT_SET_FLAGS,		// 设置玩家权限
	ECT_SET_REG_DATE,	// 设置注册时间
	ECT_SET_QQ,			// 设置QQ号
	ECT_SET_SIGNATURE,	// 设置签名
	ECT_SET_EXP_DATE,	// 设置VIP期限
	ECT_SET_LAST_LOGIN,	// 设置上次登录时间
	ECT_SET_LAST_IP,	// 设置上次登录IP
	
	// PHP接口：
	// 此函数设定的信息为UTF8格式,在PHP中为URL编码
	// user data is urlcode(utf8) saved in php
	// example: %E4%B8%AD%E5%9B%BD
	//
	// AMXX接口：
	// vs_set_user_data(1, uid, ECT_SET_NAMEEN, "user1", strlen("user1"));
	// vs_set_user_data(1, uid, ECT_SET_NAMECH, "中国人", strlen("中国人"));
	//
	// 有效范围：0-49 可自定义
	//
	
	// 以下为新增保留字段 add by MT 2023-09-13
	ECT_SET_STEAMID,	// 设置steam id
	ECT_SET_STR01,
	ECT_SET_STR02,
	ECT_SET_STR03,
	ECT_SET_STR04,
	ECT_SET_STR05,
	ECT_SET_STR06,
	ECT_SET_STR07,
	ECT_SET_STR08,
	ECT_SET_STR09,
	ECT_SET_STR10,
	ECT_SET_STR11,
	ECT_SET_STR12,
	ECT_SET_STR13,
	ECT_SET_STR14,
	ECT_SET_STR15,
}

// 设定玩家信息数据
native vs_set_user_data(id, uid, type, string[], len);



//////////////////////////////////////////////////////////////////////////
// 清空玩家排名数据
//////////////////////////////////////////////////////////////////////////
enum CLEAR_INFO
{
	ECT_CLR_ALL = 0,	// 清空全部排名	(无须日期)
	ECT_CLR_YEAR,		// 清空指定年	(年)
	ECT_CLR_MONTH,		// 清空指定月	(年+月)
	ECT_CLR_DAY,		// 清空指定日	(年+月+日)
	ECT_CLR_SESSION,	// 清空指定赛季	(年+月+日)
}

// 清空玩家排名数据信息
native vs_clear_data(id, uid, type, year, month, day);




//////////////////////////////////////////////////////////////////////////
// 回调事件
//////////////////////////////////////////////////////////////////////////

// 首次读取玩家排名信息成功
forward vs_rank_init(id, userid, rank, total);

// 当玩家玩家排名变动时
forward vs_rank_update(id, userid, oldRank, newRank, total);




// 玩家死亡事件,如果友伤则TA为1
forward vs_client_death(killer,victim,wpnindex,hitplace,TK);

// 玩家攻击事件,如果友伤则TA为1
forward vs_client_damage(attacker,victim,damage,wpnindex,hitplace,TA);

// 安装炸弹成功
forward vs_bomb_planted(planter);

// 拆除炸弹成功
forward vs_bomb_defused(defuser);

// 玩家正在安装炸弹
forward vs_bomb_planting(planter);

// 玩家正拆除炸弹
forward vs_bomb_defusing(defuser);

// 炸弹成功爆炸
forward vs_bomb_explode(planter,defuser);

// 玩家仍O345消息
forward vs_grenade_throw( index,greindex,wId);




//////////////////////////////////////////////////////////////////////////
// 新增自定义武器接口  by MT 2023-09-28
//////////////////////////////////////////////////////////////////////////
/* 函数将返回新武器的索引 */
native custom_weapon_add( const wpnname[],melee = 0,const logname[]="" ); 

/* 功能会将此自定义武器造成的伤害传递给统计模块和其他插件 */
native custom_weapon_dmg( weapon, att, vic, damage, hitplace=0 ); 

/* 函数将把有关自定义武器射击的信息传递到统计模块 */
native custom_weapon_shot( weapon,index ); // weapon id , player id

/* 如果为true，函数将返回1 */
native xmod_is_melee_wpn(wpnindex);

/* 返回武器名称 */
native xmod_get_wpnname(wpnindex,name[],len);

/* 返回武器日志名称 */
native xmod_get_wpnlogname(wpnindex,name[],len);

/* 返回武器最大个数 */
native xmod_get_maxweapons();

/* 获取排名最大个数 */
native xmod_get_stats_size();







/* AMXX-Studio Notes - DO NOT MODIFY BELOW HERE
*{\\ rtf1\\ ansi\\ ansicpg936\\ deff0{\\ fonttbl{\\ f0\\ fnil\\ fcharset134 Tahoma;}}\n\\ viewkind4\\ uc1\\ pard\\ lang2052\\ f0\\ fs16 \n\\ par }
*/
