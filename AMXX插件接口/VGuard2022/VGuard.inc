//////////////////////////////////////////////////////////////////////////
//
// VGuard反作弊器定制版 —— 插件开发文档
// 当前版本: v2.0
// 编辑时间: 2022-03-19
// 版权所有: Koma
// 定制QQ:	 277460272
// 官方网站: http://www.cs27015.com
//
//////////////////////////////////////////////////////////////////////////


// 判断玩家是否使用MiGuard
// id			玩家id
// 返回: 成功返回客户端数字版本号(20160701),否则0
native vg_getstatus(id);


// 机器码最大长度
#define MI_GUARD_LEN_MAX	33

// 获取玩家机器码
// id			玩家id
// szMac		机器码	(传入)
// len			最大长度(传入)
// 返回: 实际长度
native vg_getmac(id, szMac[], len);

//////////////////////////////////////////////////////////////////////////
//
// 转发函数
//

// 玩家登录消息
// id			玩家id
// userid		玩家userid
// ver			客户端版本号
// szUser		用户
// szPass		密码
// szMac		机器码
forward mi_guard_login(id, userid, ver, szUser[], szPass[], szMac[])

// 作弊消息
// id			玩家id
// szModule		模块名称(xx.dll)
// uType		作弊器类型
// nEbp			非法地址
// nFileCrc		文件Crc32
// nFileSize	文件大小
// nFileCode	文件Crc32
forward mi_guard_cheat(id, szModule[], uType, nEbp, nFileCrc, nFileSize, nFileCode)


// 文件检查消息
// id			玩家id
// szModule		模块名称(xx.dll)
// nFileCrc		文件Crc32
// nFileCode	文件Crc32
// nRet			检查结果(0白名单,1黑名单,2未知)
forward mi_guard_check(id, szModule[], nFileCrc, nFileCode, nRet)


//////////////////////////////////////////////////////////////////////////
// VGuard截屏
//////////////////////////////////////////////////////////////////////////

// 截图方式
// 0 游戏引擎(绝对无黑屏)
// 1 WinGDI  (有可能黑屏)
#define MI_GUARD_ENGINE		0
#define MI_GUARD_GDI		1

// 管理员申请截图
// iAdmin		管理id
// iPlayer		玩家id
// type			截图类型
// 返回: 成功1,否则0
native vg_screen(iAdmin, iPlayer, type);

//////////////////////////////////////////////////////////////////////////
// VGuard扫描模块
//////////////////////////////////////////////////////////////////////////

// 模块方式
// 0 普通扫描
// 1 深度扫描(可能会造成玩家卡顿)
#define MI_GUARD_NORMAL		0
#define MI_GUARD_DEEP		1

// 管理员申请扫描模块
// id			管理id
// player		玩家id
// type			扫描类型
// 返回: 成功1,否则0
native vg_scan(iAdmin, iPlayer, type);





// 文字通道数量 (通道数字越大的显示在顶层)
#define MAX_TEXT_CHANNEL	16

// 文字最大长度
#define MAX_TEXT_SIZE		2048

// 文字通道 —— 初始化字体
// id			玩家id
// channel		通道序号
// szFonName	字体名称, 示例:微软雅黑 "msyh.ttc"
// nFontSize	字体大小
// bBold		是否加粗
// bItalic		是否斜体
native vg_hud_create_font(id, channel, szFonName[], nFontSize, bBold, bItalic);

// 文字通道 —— 设置文字位置
// id			玩家ID
// channel		通道序号
// x			坐标X (0.0 - 100.0，负数则低于左侧)
// y			坐标Y (0.0 - 100.0，负数则低于上方)
native vg_hud_set_pos(id, channel, x, y);

// 文字通道 —— 设置文字对齐样式 (要在vg_hud_set_text之后调用)
// id			玩家ID
// channel		通道序号
// align		对齐样式
native vg_hud_set_align(id, channel, align);

// 文字通道 —— 设置文字显示时间
// id			玩家ID
// channel		通道序号
// time			显示时间(秒)
native vg_hud_set_showtime(id, channel, time);

// 文字通道 —— 设置文字滚动速度
// id			玩家ID
// channel		通道序号
// speed		滚动速度(0-10,10最快)
native vg_hud_set_speed(id, channel, speed);

// 文字通道 —— 设置文字内容 (支持换行，以\n或^x0结束)
// id			玩家ID
// channel		通道序号
// text			文字内容
// nLength		缓冲区大小
native vg_hud_set_text(id, channel, text[]);

// 文字通道 —— 是否开始显示
// id			玩家ID
// channel		通道序号
// enable		是否开始显示
native vg_hud_set_enable(id, channel, enable);



// 文字通道 —— 设置文字内容 (支持换行，以\n或^x0结束)
// id			玩家ID
// channel		通道序号
// x			坐标X (0.0 - 100.0，负数则低于左侧)
// y			坐标Y (0.0 - 100.0，负数则低于上方)
// speed		滚动速度(0-10,10最快)
// time			显示时间(秒)
// align		对齐样式
// text			文字内容
// nLength		缓冲区大小
// 注意：此函数只需要初始化字体后随时调用即可(无须enable)
native vg_hud_draw_text(id, channel, x, y, speed, time, align, text[]);


//////////////////////////////////////////////////////////////////////////
// 对齐方式
enum
{
	ETA_NONE,		// 默认不对齐
	ETA_LEFT,		// 左对齐
	ETA_RIGH,		// 右对齐
	ETA_LCENTER,	// 左居中
	ETA_RCENTER,	// 左居中
	ETA_VCENTER,	// 垂直居中
	ETA_SLEFT,		// 从左向右滚动
	ETA_SRIGHT,		// 从右向左滚动
	ETA_SUP,		// 从上向下滚动
	ETA_SDOWN,		// 从下向上滚动
	ETA_SCENTERUP,	// 居中从上向下滚动
	ETA_SCENTERDOWN,// 居中从下向上滚动
}

//////////////////////////////////////////////////////////////////////////
// 协议包 —— 作弊器类型
enum
{
	CT_OPENGL = 0x00,		// opengl32
	CT_INJECT,				// 注入器
	CT_IMAGE,				// 防截图
	CT_WALLHACK,			// 透视
	CT_SMOKE,				// 防闪烟雾
	
	CT_MOVE,				// 自瞄
	CT_ENGINE,				// Engine Hack
	CT_HACKTOOL,			// AntiHOOK
	CT_MSGBOX,				// MsgBox
	CT_SPEED,				// 加速瞬移
	
	CT_SIGHTS,				// 准星辅助
	CT_DEBUG,				// 调试进程
	CT_KEYEVENT,			// 模拟按键
	CT_MOUSEEVENT,			// 模拟鼠标
	CT_GHOST,				// 按键精灵
	
	CT_THREAD_OPENGL,		// Thread OpenGL
	CT_THREAD_ERR,			// Thread Error
	CT_VM_CLIENT,			// VM client.dll
	CT_VM_HW,				// VM hw.dll
	CT_VM_CSPUB,			// VM CSPub.dll
	
	CT_VM_ENGINE,			// VM Engine
	CT_VM_NATIVE,			// VM Native
	CT_LDRLOAD,				// 恢复LdrLoadDll
	CT_NTFUNCTION,			// 恢复NtRead/Write
	
	CT_MEMORYCRC,			// 内存校验
	CT_MEMORYRW,			// 内存读写
	
	CT_BLACK,				// 黑名单文件
	CT_UNKNOW,				// 位置原因
}



