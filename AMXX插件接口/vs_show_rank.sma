//
// VS_Show_Rank.sma
// 用来显示一些常见排名命令
// http:cs27015.com
//
#include <amxmodx>

#include "VGuard2022/VSLogin.inc"

#define PLUGIN 	"VSShowRank"
#define VERSION "3.01"
#define AUTHOR 	"Koma"

#define is_player(%1) (1<=%1<=32)

// 网站地址
#define BASE_URL	"http://game.cs27015.com/vgstats"

new g_uid[33]

public plugin_init() 
{
	register_plugin(PLUGIN, VERSION, AUTHOR)

	register_dictionary("admin.txt")
	
	register_clcmd("say /me", "cmd_stats", 0, "- display your stats (MOTD)")
	register_clcmd("say /rank", "cmd_stats", 0, "- display your server stats (MOTD)")
	register_clcmd("say /stats", "cmd_stats", 0, "- display your server stats (MOTD)")
	register_clcmd("say /rankstats", "cmd_stats", 0, "- display your server stats (MOTD)")
	register_clcmd("amx_stats_me", "cmd_stats", 0, "- display your server stats (MOTD)")
	
	register_clcmd("say /top15", "cmd_top", 0, "- display top players (MOTD)")
	register_clcmd("say /top", "cmd_top", 0, "- display top players (MOTD)")
	register_clcmd("amx_stats_top", "cmd_top", 0, "- display top players (MOTD)")
}

public cmd_stats(id)
{
	if(!is_player(id))
		return PLUGIN_HANDLED
	
	if (g_uid[id] > 0)
	{
		static full_url[256]
		formatex(full_url, charsmax(full_url), "%s/Rank.php?uid=%d&iType=all", BASE_URL, g_uid[id])
		show_motd(id, full_url,"VGStats - 个人统计")
	}
	return PLUGIN_HANDLED
}

public cmd_top(id)
{
	if (is_player(id))
	{
		static full_url[256]
		formatex(full_url, charsmax(full_url), "%s/index.php?iType=all", BASE_URL)
		show_motd(id, full_url,"VGStats - 总排行榜")
	}
	return PLUGIN_HANDLED
}

public client_putinserver(id)
{
	if(!is_player(id))
		return
	
	g_uid[id] = 0
}

public client_disconnect(id)
{
	if(!is_player(id))
		return
	
	g_uid[id] = 0
}

stock client_color(receiver, sender, const message[], {Float,Sql,Result,_}:...)
{
	if (!sender || 1 <= receiver <= 32 && !is_user_connected(receiver))
	{
		return 0
	}
	
	static msg[190]
	vformat(msg, 189, message, 4)
	message_begin(receiver ? MSG_ONE_UNRELIABLE : MSG_ALL, get_user_msgid("SayText"), {0, 0, 0}, receiver)
	write_byte(sender)
	write_string(msg)
	message_end()
	
	return 1
}

public client_login(id, uid, exp, type)
{
	if(!is_player(id))
		return
	
	g_uid[id] = uid
}
