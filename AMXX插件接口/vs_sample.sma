//
// vs_sample.sma
// 如果你用到MTLogin自动注册登录,就可以参照这个写
// 这是一个VGStats接口调用的示例代码
// http:cs27015.com
//
#include <amxmodx>

#include "VGuard2022/VGStats.inc"
#include "VGuard2022/VSLogin.inc"

#define PLUGIN 	"VSSample"
#define VERSION "3.01"
#define AUTHOR 	"Koma"

#define is_player(%1) 	(1<=%1<=32)

// 调试模式
#define DEBUG			0

new g_maxplayers
new g_Uid[33]


// 根据userid获取实际下标
stock get_user_real_index(uid)
{
	for(new i=1; i<g_maxplayers; i++)
	{
		if(!is_user_connected(i))
			continue;
		
		if(uid == get_user_userid(i))
			return i;
	}
	
	return 0;
}

// 当玩家玩家排名变动时
public vs_rank_update(id, userid, oldRank, newRank, total)
{
	new idx
	if(!is_player(id))
		return
	
	idx = get_user_real_index(userid)
	if(!is_player(id))
		return
	
	if(newRank > oldRank)
	{
		client_print(idx, print_chat, "[VGStats] 要猥琐发育哦，你的总排名已经从第(%d)掉落至第(%d/%d)名了!", oldRank, newRank, total)
	}
	else
	{
		client_print(idx, print_chat, "[VGStats] 恭喜！你的总排名已经从第(%d)提升至第(%d/%d)名了!", oldRank, newRank, total)
	}
}


// 当加载玩家排名信息完毕时
public vs_rank_init(id, userid, rank, total)
{
	new idx
	if(!is_player(id))
		return
	
	idx = get_user_real_index(userid)
	if(!is_player(id))
		return
	
	client_print(idx, print_chat, "[VGStats] 你的排名数据加载成功，当前总榜第(%d/%d)名!", rank, total)
}


public client_connect(id) 
{
	if(!is_player(id))
		return
	
	g_Uid[id] = 0
}

public cmd_vsload(id)
{
	new	dbid
	if(!is_player(id))
		return

	dbid = vs_get_uid(id)
	if(dbid == 0)
	{
		client_print(id, print_chat, "[VGStats] 你的数字编号为空，还未登录成功！")
		return
	}
	
	vs_load_rank(id, dbid)
}

public cmd_vsrank(id)
{
	//new data[32]
	
	if(!is_player(id))
		return
	
	// 这是旧版V2.0接口函数
	//vs_get_stats(id, data)
	//client_print(id, print_chat, "[VGStats] v2.0 你的总排名: %d / %d,  杀敌:%d,  射击:%d,  阵亡:%d ", data[VS_RANK], data[VS_MAX_PLAYER], data[VS_KILL], data[VS_SHOT], data[VS_DEATH])
	
	// 以下是新版V3.0接口函数
	new nRank = vs_get_rank_data(id, CSW_NONE, VS_RANK);
	new nMaxPlayer = vs_get_rank_data(id, CSW_NONE, VS_MAX_PLAYER);
	new nKill = vs_get_rank_data(id, CSW_NONE, VS_KILL);
	new nDeath = vs_get_rank_data(id, CSW_NONE, VS_DEATH);
	new nShot = vs_get_rank_data(id, CSW_NONE, VS_SHOT);
	client_print(id, print_chat, "[VGStats] v3.0 你的总排名: %d / %d,  杀敌:%d,  射击:%d,  阵亡:%d ", nRank, nMaxPlayer, nKill, nShot, nDeath)
}


// MTLogin主插件在每次自动登录或注册成功后,会调用此函数
public client_login(id, uid, exp, type)
{
	static param[4]
	param[0] = id
	param[1] = get_user_userid(id)
	param[2] = uid
	param[3] = type
	
	new enname[33], chs[33], sig[64], qq[33], flag[33], expdate[33], regdate[33], lastlogin[33], ip[33]
	new year, month, day
	
	g_Uid[id] = uid
	
	// 获取玩家英文昵称
	get_user_engname(id, enname, 32)
	
	// 获取玩家中文昵称
	get_user_chsname(id, chs, 32)
	
	// 获取玩家签名
	get_user_signature(id, sig, 63)
	
	// 获取玩家QQ
	get_user_qq(id ,qq, 32)
	
	// 获取VIP字符串
	get_vip_flag(id, flag, 32)
	
	// 获取VIP期限
	get_exp_date(id, expdate, 32)
	
	// 获取注册日期
	get_reg_date(id, regdate, 32)
	
	date(year, month, day)
	formatex(lastlogin, 31, "%04d-%02d-%02d", year, month, day)
	get_user_ip(id, ip, 32, 1)

	// 设定玩家基本信息
	vs_set_user_data(id, uid, ECT_SET_NAMEEN, enname, 32)
	vs_set_user_data(id, uid, ECT_SET_NAMECH, chs, 32)
	vs_set_user_data(id, uid, ECT_SET_FLAGS, flag, 32)
	vs_set_user_data(id, uid, ECT_SET_REG_DATE, regdate, 32)
	vs_set_user_data(id, uid, ECT_SET_QQ, qq, 32)
	vs_set_user_data(id, uid, ECT_SET_SIGNATURE, sig, 32)
	
	// 加载玩家所有武器数据
	vs_load_rank(id, uid)
	
	if(strlen(expdate))
		vs_set_user_data(id, uid, ECT_SET_EXP_DATE, expdate, 32)
	else
		vs_set_user_data(id, uid, ECT_SET_EXP_DATE, expdate, 0)
		
	vs_set_user_data(id, uid, ECT_SET_LAST_LOGIN, lastlogin, 32)
	vs_set_user_data(id, uid, ECT_SET_LAST_IP, ip, 32)
	
	#if DEBUG
	client_print(id, print_chat, "提交: uid:%u name:%s / %s / %s / %s", uid, enname, chs, sig, ip)
	server_print("id:%d - %s - %s - %s client_login", id, enname, flag, expdate)
	#endif
}

public cmd_vsclear(id)
{
	new	uid = 0
	if(!is_player(id))
		return
	
	uid = vs_get_uid(id)
	if(uid == 0)
	{
		client_print(id, print_chat, "[VGStats] 你的数字编号为空，还未登录成功！")
		return
	}
	
	vs_clear_data(id, uid, ECT_CLR_ALL, 0, 0, 0)
}

public cmd_vsak(id)
{
	new	uid = 0
	new wkill, whead, wshot, whit
	new wid = CSW_AK47
	
	if(!is_player(id))
		return
	
	uid = vs_get_uid(id)
	if(uid == 0)
	{
		client_print(id, print_chat, "[VGStats] 你的数字编号为空，还未登录成功！")
		return
	}
	
	// 武器数据
	wkill = vs_get_rank_data(id, wid, VS_KILL);
	whead = vs_get_rank_data(id, wid, VS_HEADSHOT);
	wshot = vs_get_rank_data(id, wid, VS_SHOT);
	whit = vs_get_rank_data(id, wid, VS_HIT);
				
	client_print(id, print_chat, "[VGStats] AK47 - 杀敌%d, 爆头:%d, 射击:%d, 命中:%d", wkill, whead, wshot, whit)
}

public plugin_init()
{
	register_plugin( PLUGIN, VERSION, AUTHOR);
	register_clcmd("say /vsload", "cmd_vsload")
	register_clcmd("say /vsrank", "cmd_vsrank")
	register_clcmd("say /vsclear", "cmd_vsclear")
	register_clcmd("say /vsak", "cmd_vsak")
	
	g_maxplayers = get_maxplayers()
	if (g_maxplayers > 32)
	{	
		g_maxplayers = 32
	}
} 

