# VGStats

#### Description
 你是否在为用MySQL做排名系统导致的换图慢死服而苦恼？恭喜你，这款基于高效率Redis数据库的排名系统VGStats将非常适合你，它采用高速缓存技术，无论是读取还是个人数据读取、计算并显示前一千名也仅仅只需要0.01秒左右！
 这是一款基于Redis + PHP的CS1.6排名统计系统，使用于HLDS系列的游戏，本插件完全免费，如果遇到什么bug请加官方QQ群：529777716

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
