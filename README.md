# VGStats

#### 更新时间
当前版本：V3.01(全兼容旧版数据)  
更新时间：2023-09-25 23:17  
1.增加steam id、备用字段接口  
2.增加武器击中每个部位的次数统计  
3.增加支持Linux+ReHLDS  

新版预览：http://game.cs27015.com 

#### 功能介绍
基于Redis+PHP的CS1.6排名统计系统，使用于HLDS系列的混战、比赛模式

#### 安装方法

旧版预览测试效果：http://game.cs27015.com/vgstats/

官方网站：http://www.cs27015.com
专业定制CS反作弊系统QQ277460272


安装方法如下：

1、在cstrike\server.cfg中添加以下参数：

```cpp
   // 排名网关服务器  
   vs_ip			"127.0.0.1"  
   vs_port			"27001"  

   // 排名网关验证密码
   vs_auth			"123456"  

   // 当前服务器序号(一个网关可以对应多个不同模式排名)
   vs_sid			"1"  

   // 是否忽略BOT
   vs_ignore_bot	"0"  

   // 实时刷新全部玩家排名时间间隔(秒)   
   vs_update_rank_sec	"60.0"  
   
   // 如果不使用VSLogin.amxx自动注册登录系统,则不需要此参数
   // MySQL参数
   rpgm_sql_host	"127.0.0.1"
   rpgm_sql_user 	"root"
   rpgm_sql_pass 	"123456"
   rpgm_sql_db 		"hlds_vguard"
```

2、将VGStats_amxx.dll拷贝至以下目录：
```cpp
   cstrike\addons\amxmodx\modules
``` 

3、在module.ini中添加一行VGStats
```cpp
   cstrike\addons\amxmodx\configs\module.ini
``` 

4、重启HLDS服务器，输入meta list查看是否成功
   如果有一个VGStats System显示RUN则说明正常
   
   
5、运行“启动redis.bat”

6、运行VGCenter.exe点启动即可

7、Redis使用的是tcp:6379端口，VGCenter使用到udp:27001端口
   如果Redis或VGCenter在其他服务器运行，需要注意放行！

8、将wwwroot拷贝至服务器运行，测试环境为PHP7.0，其他版本未测试
   需要自行修改MTPublic.php中的get_access_str获取VIP类型
   
   
#### 网页预览
// 默认显示总榜
http://game.cs27015.com/vgstats/index.php
http://game.cs27015.com/vgstats/index.php?iType=all

// 年榜
http://game.cs27015.com/vgstats/index.php?iType=year&iDate=2023

// 月榜
http://game.cs27015.com/vgstats/index.php?iType=month&iDate=202310

// 日榜
https://game.cs27015.com/vgstats/index.php?iType=day&iDate=20231008

=============================================

// 显示个人总榜
http://game.cs27015.com/vgstats/rank.php?uid=19
http://game.cs27015.com/vgstats/rank.php?uid=19&iType=all

// 显示个人年榜
http://game.cs27015.com/vgstats/rank.php?uid=19&iType=year&iDate=2023

// 显示个人月榜
http://game.cs27015.com/vgstats/rank.php?uid=19&iType=month&iDate=202310

// 显示个人日榜
http://game.cs27015.com/vgstats/rank.php?uid=19&iType=day&iDate=20231008


#### Bug反馈
   本插件完全免费，如果遇到什么bug请加官方QQ群：529777716
   (如果有哪位兄弟熟悉PHP可联系我共同改善一个WEB前端效果！)
   VGuard/VGStats交流QQ群：529777716
   http://www.cs27015.com
  
