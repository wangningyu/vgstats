<?
	$max_level = 100;
	
	$base_exp[0] = 0;
    $base_exp[1] = 210;
    $base_exp[2] = 442;
    $base_exp[3] = 697;
    $base_exp[4] = 977;
    $base_exp[5] = 1284;
    $base_exp[6] = 1621;
    $base_exp[7] = 1988;
    $base_exp[8] = 2389;
    $base_exp[9] = 2826;
    $base_exp[10] = 3301;
    $base_exp[11] = 3818;
    $base_exp[12] = 4379;
    $base_exp[13] = 4988;
    $base_exp[14] = 5648;
    $base_exp[15] = 6362;
    $base_exp[16] = 7135;
    $base_exp[17] = 7971;
    $base_exp[18] = 8874;
    $base_exp[19] = 9848;
    $base_exp[20] = 10899;
    $base_exp[21] = 12033;
    $base_exp[22] = 13254;
    $base_exp[23] = 14568;
    $base_exp[24] = 15983;
    $base_exp[25] = 17505;
    $base_exp[26] = 19141;
    $base_exp[27] = 20899;
    $base_exp[28] = 22787;
    $base_exp[29] = 24814;
    $base_exp[30] = 26989;
    $base_exp[31] = 29322;
    $base_exp[32] = 31824;
    $base_exp[33] = 34505;
    $base_exp[34] = 37378;
    $base_exp[35] = 40455;
    $base_exp[36] = 43750;
    $base_exp[37] = 47276;
    $base_exp[38] = 51050;
    $base_exp[39] = 55086;
    $base_exp[40] = 59403;
    $base_exp[41] = 64017;
    $base_exp[42] = 68949;
    $base_exp[43] = 74220;
    $base_exp[44] = 79849;
    $base_exp[45] = 85862;
    $base_exp[46] = 92281;
    $base_exp[47] = 99133;
    $base_exp[48] = 106447;
    $base_exp[49] = 114250;
    $base_exp[50] = 122574;
    $base_exp[51] = 131451;
    $base_exp[52] = 140918;
    $base_exp[53] = 151010;
    $base_exp[54] = 161768;
    $base_exp[55] = 173233;
    $base_exp[56] = 185448;
    $base_exp[57] = 198462;
    $base_exp[58] = 212324;
    $base_exp[59] = 227087;
    $base_exp[60] = 242806;
    $base_exp[61] = 259541;
    $base_exp[62] = 277354;
    $base_exp[63] = 296314;
    $base_exp[64] = 316490;
    $base_exp[65] = 337957;
    $base_exp[66] = 360794;
    $base_exp[67] = 385087;
    $base_exp[68] = 410923;
    $base_exp[69] = 438398;
    $base_exp[70] = 467612;
    $base_exp[71] = 498671;
    $base_exp[72] = 531687;
    $base_exp[73] = 566780;
    $base_exp[74] = 604076;
    $base_exp[75] = 643708;
    $base_exp[76] = 685818;
    $base_exp[77] = 730557;
    $base_exp[78] = 778083;
    $base_exp[79] = 828565;
    $base_exp[80] = 882181;
    $base_exp[81] = 939119;
    $base_exp[82] = 999580;
    $base_exp[83] = 1063775;
    $base_exp[84] = 1131928;
    $base_exp[85] = 1204277;
    $base_exp[86] = 1281073;
    $base_exp[87] = 1362582;
    $base_exp[88] = 1449086;
    $base_exp[89] = 1540882;
    $base_exp[90] = 1638287;
    $base_exp[91] = 1741634;
    $base_exp[92] = 1851276;
    $base_exp[93] = 1967589;
    $base_exp[94] = 2090967;
    $base_exp[95] = 2221830;
    $base_exp[96] = 2360622;
    $base_exp[97] = 2507812;
    $base_exp[98] = 2663896;
    $base_exp[99] = 2829400;
    $base_exp[100] = 3000000;
	
    $military_base[0] = '小孩';
    $military_base[1] = '军校生';
    $military_base[2] = '毕业生';
    $military_base[3] = '见习生';
    $military_base[4] = '新兵';
    $military_base[5] = '列兵';
    $military_base[6] = '一等兵';
    $military_base[7] = '下士';
    $military_base[8] = '中士';
    $military_base[9] = '上士';
    $military_base[10] = '参谋军士';
    $military_base[11] = '军士长';
    $military_base[12] = '少尉副排';
    $military_base[13] = '少尉排长';
    $military_base[14] = '中尉副连';
    $military_base[15] = '中尉连长';
    $military_base[16] = '中尉副营';
    $military_base[17] = '中尉营长';
    $military_base[18] = '上尉副团';
    $military_base[19] = '上尉团长';
    $military_base[20] = '少校副旅';
    $military_base[21] = '少校旅长';
    $military_base[22] = '中校副师';
    $military_base[23] = '中校师长';
    $military_base[24] = '上校副参';
    $military_base[25] = '上校总参';
    $military_base[26] = '少将副军';
    $military_base[27] = '少将军长';
    $military_base[28] = '中将副司';
    $military_base[29] = '中将司令';
    $military_base[30] = '上将防长';
    $military_base[31] = '元帅';
    $military_base[32] = '大元帅';
    $military_base[33] = '统帅';

function get_online_string($second)
{
	$onlinestr = '0分';
	if($second < 60)
	{
		$onlinestr = sprintf("%02u秒", $second);
	}
	else if($second < 360 && $second >= 60)
	{
		$onlinestr = sprintf("%02u分%02u秒", round($second / 60), round($second % 60));
	}
	else
	{
		$onlinestr = sprintf("%02u时%02u分%02u秒", round($second / 360), round($second % 360 / 60), round($second % 60));
	}
	
	return $onlinestr;
}

// 获取玩家登记
function get_user_level($score)
{
	if($score <= 0)
		return 0;
	
	global $base_exp;
	for ($i= 1; $i<100; $i++)
	{
		if($base_exp[$i] >= $score)
			return $i - 1;
	}
}

// 获取玩家头衔
function get_user_military($level)
{
	global $military_base;
	$military = $military_base[0];
	if ($level == 0)
        return $military;
    else if($level < 6)
        return $military_base[1];
    else if($level < 10)
        return $military_base[2];
    else if($level == 100)
        return $military_base[99];
    else
        return $military_base[$level / 3];
}

?>