<?php

//https://www.cnblogs.com/itbsl/p/13405238.html
//https://www.runoob.com/redis/sorted-sets-zlexcount.html
//https://github.com/owlient/phpredis/blob/master/php_redis.h
//https://www.jb51.net/article/51886.htm

// 榜单类型：全部、年、月
#define RANK_ALL		0
#define RANK_YEAR		1
#define RANK_MONTH		2

require_once ("MTPublic.php");
require_once ("Military.php");

class CRankHeader{
	var $m_vType;			// 榜单类型
	var $m_nMaxPlayer;		// 最大人数
	var $m_nStart;			// 开始
	var $m_nEnd;			// 结束

	var $m_arrRank;			// 排名
	var $m_arrUid;			// UID
	var $m_arrScore;		// 总积分
	
	//+1kill, +2shot, +3head, +4hit, +5damge, +6death, +7fkill, +8fdeath, +9defu, +10plant, +11online
	var $m_arrKill;			// 杀敌
	var $m_arrShot;			// 射击
	var $m_arrHead;			// 爆头
	var $m_arrHit;			// 击中
	var $m_arrDamage;		// 伤害
	var $m_arrDeath;		// 阵亡
	var $m_arrFirstKill;	// 首杀
	var $m_arrFirstDeath;	// 首死
	var $m_arrDefu;			// 拆除炸弹
	var $m_arrPlant;		// 安装炸弹
	
	// 新增回合场次相关选项 add by MT 2023-10-01
	var $m_arr1k;			// 1k
	var $m_arr2k;			// 2k
	var $m_arr3k;			// 3k
	var $m_arr4k;			// 4k
	var $m_arr5k;			// 5k
	var $m_arr6k;			// 6k
	var $m_arr7k;			// 7k
	var $m_arr8k;			// 8k
	var $m_arr9k;			// 9k
	var $m_arr10k;			// 10k
	var $m_arr11k;			// 11k
	var $m_arr12k;			// 12k
	var $m_arr13k;			// 13k
	var $m_arr14k;			// 14k
	var $m_arr15k;			// 15k
	
	var $m_arr1v1;			// 1v1
	var $m_arr1v2;			// 1v2
	var $m_arr1v3;			// 1v3
	var $m_arr1v4;			// 1v4
	var $m_arr1v5;			// 1v5
	var $m_arr1v6;			// 1v6
	var $m_arr1v7;			// 1v7
	var $m_arr1v8;			// 1v8
	var $m_arr1v9;			// 1v9
	var $m_arr1v10;			// 1v10
	var $m_arr1v11;			// 1v11
	var $m_arr1v12;			// 1v12
	var $m_arr1v13;			// 1v13
	var $m_arr1v14;			// 1v14
	var $m_arr1v15;			// 1v15
	
	var $m_arrAssist;		// 助攻
	var $m_arrMvp;			// MVP
	var $m_arrWin;			// 总胜率
	var $m_arrWinT;			// T胜率
	var $m_arrWinCT;		// CT胜率
	
	var $m_arrRWS;			// RWS
	var $m_arrAdr;			// ADR
	var $m_arrRating;		// Rating
	
	// 总穿墙数据
	var $m_arrWallHit;		// 穿墙命中次数
	var $m_arrWallDamage;	// 穿墙射击伤害
	var $m_arrWallHead;		// 穿墙爆头次数
	var $m_arrWallKill;		// 穿墙击杀次数
	
	var $m_arrBWallHit;		// 被穿墙命中次数
	var $m_arrBWallDamage;	// 被穿墙射击伤害
	var $m_arrBWalHead;		// 被穿墙爆头次数
	var $m_arrBWallKill;	// 被穿墙击杀次数
	
	var $m_arrOnline;		// 在线时间
	var $m_arrKD;			// KD
	var $m_arrHdKD;			// 爆头率
	var $m_arrShotPeer;		// 命中率
	var $m_arrLevel;		// 当前等级
	var $m_arrMilitary;		// 头衔
	
	var $m_arrChsName;		// 中文名
	var $m_arrVipFlags;		// VIP字母
	var $m_arrVipStr;		// VIP权限
	var $m_arrExpDate;		// VIP期限
	var $m_Access;			// 权限字母
	var $m_ExpDate;			// VIP期限
	
	var $m_iDate;			// 查询日期
	var $m_pRedis;

	function __construct($pRedis, $vType, $iDate, $page, $perCount)
	{
		$this->m_nMaxPlayer = 0;
		$this->m_vType = $vType;
		$this->m_iDate = $iDate;
		$this->m_nStart = ($page - 1) * $perCount;
		$this->m_nEnd = $this->m_nStart + $perCount;
		$this->m_pRedis = $pRedis;
	}

	// 获取数字编号
	function get_uid($iPos)
	{
		if(count($this->m_arrUid))
			return $this->m_arrUid[$iPos];
		else
			return 0;
	}
	
	// 获取杀敌
	function get_kill($iPos)
	{
		if(count($this->m_arrKill))
			return $this->m_arrKill[$iPos];
		else
			return 0;
	}

	// 获取死亡
	function get_death($iPos)
	{
		if(count($this->m_arrDeath))
			return $this->m_arrDeath[$iPos];
		else
			return 0;
	}
	
	// 获取射击次数
	function get_shot($iPos)
	{
		if(count($this->m_arrShot))
			return $this->m_arrShot[$iPos];
		else
			return 0;
	}

	// 获取命中次数
	function get_hit($iPos)
	{
		if(count($this->m_arrHit))
			return $this->m_arrHit[$iPos];
		else
			return 0;
	}
	
	// 获取爆头
	function get_head($iPos)
	{
		if(count($this->m_arrHead))
			return $this->m_arrHead[$iPos];
		else
			return 0;
	}

	// 获取首杀
	function get_firstKill($iPos)
	{
		if(count($this->m_arrFirstKill))
			return $this->m_arrHead[$iPos];
		else
			return 0;
	}

	// 获取首死
	function get_firstDeath($iPos)
	{
		if(count($this->m_arrFirstDeath))
			return $this->m_arrFirstDeath[$iPos];
		else
			return 0;
	}

	// 获取安装炸弹
	function get_plant($iPos)
	{
		if(count($this->m_arrPlant))
			return $this->m_arrPlant[$iPos];
		else
			return 0;
	}
	
	// 获取1k
	function get_1k($iPos)
	{
		if(count($this->m_arr1k))
			return $this->m_arr1k[$iPos];
		else
			return 0;
	}
	
	// 获取2k
	function get_2k($iPos)
	{
		if(count($this->m_arr2k))
			return $this->m_arr2k[$iPos];
		else
			return 0;
	}
	
	// 获取3k
	function get_3k($iPos)
	{
		if(count($this->m_arr3k))
			return $this->m_arr3k[$iPos];
		else
			return 0;
	}
	
	// 获取3k
	function get_4k($iPos)
	{
		if(count($this->m_arr4k))
			return $this->m_arr4k[$iPos];
		else
			return 0;
	}
	
	// 获取5k
	function get_5k($iPos)
	{
		if(count($this->m_arr5k))
			return $this->m_arr5k[$iPos];
		else
			return 0;
	}
	
	// 获取1v1
	function get_1v1($iPos)
	{
		if(count($this->m_arr1v1))
			return $this->m_arr1v1[$iPos];
		else
			return 0;
	}
	
	// 获取1v2
	function get_1v2($iPos)
	{
		if(count($this->m_arr1v2))
			return $this->m_arr1v2[$iPos];
		else
			return 0;
	}
	
	// 获取1v3
	function get_1v3($iPos)
	{
		if(count($this->m_arr1v3))
			return $this->m_arr1v3[$iPos];
		else
			return 0;
	}
	
	// 获取1v4
	function get_1v4($iPos)
	{
		if(count($this->m_arr1v4))
			return $this->m_arr1v4[$iPos];
		else
			return 0;
	}
	
	// 获取1v5
	function get_1v5($iPos)
	{
		if(count($this->m_arr1v5))
			return $this->m_arr1v5[$iPos];
		else
			return 0;
	}
	
	// 获取拆除炸弹
	function get_defu($iPos)
	{
		if(count($this->m_arrDefu))
			return $this->m_arrDefu[$iPos];
		else
			return 0;
	}
	
	// 获取助攻
	function get_assist($iPos)
	{
		if(count($this->m_arrAssist))
			return $this->m_arrAssist[$iPos];
		else
			return 0;
	}

	// 获取总胜率
	function get_win($iPos)
	{
		if(count($this->m_arrWin))
			return number_format($this->m_arrWin[$iPos], 1);
		else
			return 0;
	}

	// 获取T胜率
	function get_winT($iPos)
	{
		if(count($this->m_arrWinT))
			return number_format($this->m_arrWinT[$iPos], 1);
		else
			return 0;
	}
	
	// 获取CT胜率
	function get_winCT($iPos)
	{
		if(count($this->m_arrWinCT))
			return number_format($this->m_arrWinCT[$iPos], 1);
		else
			return 0;
	}
	
	// 获取MVP
	function get_mvp($iPos)
	{
		if(count($this->m_arrMvp))
			return $this->m_arrMvp[$iPos];
		else
			return 0;
	}
	
	// 获取ADR
	function get_adr($iPos)
	{
		if(count($this->m_arrAdr))
			return number_format($this->m_arrAdr[$iPos], 1);
		else
			return (float)0;
	}
	
	// 获取RWS
	function get_rws($iPos)
	{
		if(count($this->m_arrRWS))
			return number_format($this->m_arrRWS[$iPos], 1);
		else
			return (float)0;
	}
	
	// Rating
	function get_rating($iPos)
	{
		if(count($this->m_arrRating))
			return number_format($this->m_arrRating[$iPos], 3);
		else
			return (float)0;
	}
	
	// 穿墙命中次数 
	function get_wall_hit($iPos)
	{
		if(count($this->m_arrWallHit))
			return $this->m_arrWallHit[$iPos];
		else
			return 0;
	}
	
	// 穿墙射击伤害 
	function get_wall_damage($iPos)
	{
		if(count($this->m_arrWallDamage))
			return $this->m_arrWallDamage[$iPos];
		else
			return 0;
	}
	
	// 穿墙爆头次数 
	function get_wall_head($iPos)
	{
		if(count($this->m_arrWallHead))
			return $this->m_arrWallHead[$iPos];
		else
			return 0;
	}
	
	// 穿墙击杀次数 
	function get_wall_kill($iPos)
	{
		if(count($this->m_arrWallKill))
			return $this->m_arrWallKill[$iPos];
		else
			return 0;
	}
	
	//////////////////////////////////////////////////////////////////
	// 被穿墙命中次数 
	function get_bwall_hit($iPos)
	{
		if(count($this->m_arrBWallHit))
			return $this->m_arrBWallHit[$iPos];
		else
			return 0;
	}
	
	// 被穿墙射击伤害 
	function get_bwall_damage($iPos)
	{
		if(count($this->m_arrBWallDamage))
			return $this->m_arrBWallDamage[$iPos];
		else
			return 0;
	}
	
	// 被穿墙爆头次数 
	function get_bwall_head($iPos)
	{
		if(count($this->m_arrBWallHead))
			return $this->m_arrBWallHead[$iPos];
		else
			return 0;
	}
	
	// 被穿墙击杀次数 
	function get_bwall_kill($iPos)
	{
		if(count($this->m_arrBWallKill))
			return $this->m_arrBWallKill[$iPos];
		else
			return 0;
	}
	
	// 获取在线时间
	function get_online($iPos)
	{
		if(count($this->m_arrOnline))
			return $this->m_arrOnline[$iPos];
		else
			return 0;
	}
	
	// 获取当前积分
	function getScore($iPos)
	{
		if(count($this->m_arrScore))
			return $this->m_arrScore[$iPos];
		else
			return 0;
	}
	
	// 获取KD
	function getKD($iPos)
	{
		if(count($this->m_arrKD))
			return $this->m_arrKD[$iPos];
		else
			return 0;
	}
	
	// 获取爆头率
	function getHdKD($iPos)
	{
		if(count($this->m_arrHdKD))
			return number_format($this->m_arrHdKD[$iPos], 1);
		else
			return 0;
	}
	
	// 获取命中率
	function getShotKD($iPos)
	{
		if(count($this->m_arrShotPeer))
			return number_format($this->m_arrShotPeer[$iPos], 1);
		else
			return 0;
	}
	
	// 获取等级
	function getLevel($iPos)
	{
		if(count($this->m_arrLevel))
			return $this->m_arrLevel[$iPos];
		else
			return 0;
	}

	// 获取头衔
	function getTitle($iPos)
	{
		if(count($this->m_arrMilitary))
			return $this->m_arrMilitary[$iPos];
		else
			return "小屁孩";
	}
	
	// 获取中文名
	function getChsName($iPos)
	{
		if(count($this->m_arrChsName))
			return $this->m_arrChsName[$iPos];
		else
			return "VGUser";
	}
	
	// 获取VIP权限
	function getVipStr($iPos)
	{
		if(count($this->m_arrVipStr))
			return $this->m_arrVipStr[$iPos];
		else
			return "普通会员";
	}

	// 获取排名范围
	function getRankStart()
	{
		return $this->m_nStart + 1;
	}
	function getRankEnd()
	{
		return $this->m_nEnd;
	}
	
	// 第三方接口：获取玩家VIP字母
	// 这里需要根据自己的实际情况修改
	/*function load_all_user()
	{
		$uidcnt  = count($this->m_arrUid);
		for($i=0;$i<$uidcnt;$i++)
		{
			mysql_query('set names utf8');
			if($i == 0)
				$scmd = sprintf("select `chsname`, `access`, `exp_date` from `mt_reg` where `index`=%s limit 1;", "1");
			else
				$scmd = sprintf("select `chsname`, `access`, `exp_date` from `mt_reg` where `index`=%s limit 1;", $m_arrUid[$i]);
			$ret = mysql_query($scmd);
			$cnt = mysql_num_rows($ret);
			if($cnt == 0)
			{
				$this->m_arrChsName[$i] = "VGuard";
				$this->m_arrVipStr[$i] = "普通会员";
				continue;
			}
			
			$row = mysql_fetch_array($ret);
			$this->m_arrChsName[$i] = $row['chsname'];
			$this->m_Access[$i] = $row['access'];
			$this->m_ExpDate[$i] = $row['exp_date'];
			$this->m_arrVipStr[$i] = get_access_str($this->m_Access[$i], $this->m_ExpDate[$i]);
		}
	}*/
	
	// 从Redis读取最大人数
	function getMaxPlayer()
	{
		if($this->m_nMaxPlayer == 0)
		{
			$scoreKey 			= getKey($this->m_vType, $this->m_iDate);
			$this->m_nMaxPlayer = $this->m_pRedis->zcard($scoreKey);

			if($this->m_nEnd >= $this->m_nMaxPlayer)
				$this->m_nEnd = $this->m_nMaxPlayer;
		}
		return $this->m_nMaxPlayer;
	}

	// HGET user:1 name xxx
	var $STATS_USER		= 'user';	// 玩家账号信息
	var $USER_NAME		= 'name';	// 设置英文名
	var $USER_CHSNAME	= 'chs';	// 设置中文名
	var $USER_FLAGS		= 'flags';	// 设置玩家权限
	var $USER_REGDATE	= 'regdate';// 设置注册时间
	var $USER_QQ		= 'qq';		// 设置QQ号
	var $USER_SIG		= 'sig';	// 设置签名
	var $USER_EXPDATE	= 'expdate';// VIP期限

	// 以下为保留字段 add by MT 2023-09-13
	var $USER_STEAMID	= "steamid";// 设置steam id
	var $USER_VS01		= "vs01";	// 1号保留字符串
	var $USER_VS02		= "vs02";
	var $USER_VS03		= "vs03";
	var $USER_VS04		= "vs04";
	var $USER_VS05		= "vs05";
	var $USER_VS06		= "vs06";
	var $USER_VS07		= "vs07";
	var $USER_VS08		= "vs08";
	var $USER_VS09		= "vs09";
	var $USER_VS10		= "vs10";
	var $USER_VS11		= "vs11";
	var $USER_VS12		= "vs12";
	var $USER_VS13		= "vs13";
	var $USER_VS14		= "vs14";
	var $USER_VS15		= "vs15";
	
	// 从Redis读取玩家基本数据
	function loadUserInfo()
	{
		$keyMain = getKey($this->m_vType, $this->m_iDate);
		$uidcnt  = count($this->m_arrUid);

		for($i=0;$i<$uidcnt;$i++)
		{
			$keyPlayer = sprintf("%s:%u", $this->STATS_USER, $this->m_arrUid[$i]);
			$res = $this->m_pRedis->hGetAll($keyPlayer);
			$resKeys  = array_keys($res);
			$resValues = array_values($res);

			$keyInfo = sprintf('%s', $this->USER_CHSNAME);
			$this->m_arrChsName[$i] = 'VGuard';
			if(array_key_exists($keyInfo, $res))
				$this->m_arrChsName[$i] = urldecode($res[$keyInfo]);

			$keyInfo = sprintf('%s', $this->USER_EXPDATE);
			$this->m_arrExpDate[$i] = '';
			if(array_key_exists($keyInfo, $res))
				$this->m_arrExpDate[$i] = urldecode($res[$keyInfo]);
			
			$keyInfo = sprintf('%s', $this->USER_FLAGS);
			$this->m_arrVipFlags[$i] = '';
			if(array_key_exists($keyInfo, $res))
				$this->m_arrVipFlags[$i] = urldecode($res[$keyInfo]);
			
			$this->m_arrVipStr[$i] = get_access_str($this->m_arrVipFlags[$i], $this->m_arrExpDate[$i]);
			
			// 以下为保留字段(范围01-15) add by MT 2023-09-13
			// steam id
			$keyInfo = sprintf('%s', $this->USER_STEAMID);
			$this->m_SteamId = $this->m_RegDate ;
			if(array_key_exists($keyInfo, $res))
				$this->m_SteamId = urldecode($res[$keyInfo]);

			// 1号保留字符串
			$keyInfo = sprintf('%s', $this->USER_VS01);
			$this->m_VS01 = 'unknow';
			if(array_key_exists($keyInfo, $res))
				$this->m_VS01 = urldecode($res[$keyInfo]);
		}
	}
	
	// 从Redis读取玩家基本数据
	function loadBase()
	{
		$keyMain = getKey($this->m_vType, $this->m_iDate);
		$uidcnt  = count($this->m_arrUid);

		for($i=0;$i<$uidcnt;$i++)
		{
			$weapon = WEAPON_UNKNOW;
			$type = STATS_UNKNOW;

			$keyPlayer = sprintf("%s:%u", $keyMain, $this->m_arrUid[$i]);
			$res = $this->m_pRedis->hGetAll($keyPlayer);
			$resKeys  = array_keys($res);
			$resValues = array_values($res);

			//print_r($resKeys);
			//echo "</br></br></br>";
			//print_r($resValues);
			//echo "</br></br></br>";
			
			// kill
			$keyInfo = sprintf('%c%c', $weapon, STATS_KILL);
			$this->m_arrKill[$i] = 0;
			if(array_key_exists($keyInfo, $res))
				$this->m_arrKill[$i] = $res[$keyInfo];
			
			// shot
			$keyInfo = sprintf('%c%c', $weapon, STATS_SHOT);
			$this->m_arrShot[$i] = 0;
			if(array_key_exists($keyInfo, $res))
				$this->m_arrShot[$i] = $res[$keyInfo];

			// head
			$keyInfo = sprintf('%c%c', $weapon, STATS_HEADSHOT);
			$this->m_arrHead[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrHead[$i] = $res[$keyInfo];
			}
			
			// hit
			$keyInfo = sprintf('%c%c', $weapon, STATS_HIT);
			$this->m_arrHit[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrHit[$i] = $res[$keyInfo];
			}
			
			// damge
			$keyInfo = sprintf('%c%c', $weapon, STATS_DAMAGE);
			$this->m_arrDamage[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrDamage[$i] = $res[$keyInfo];
			}
			
			// death
			$keyInfo = sprintf('%c%c', $weapon, STATS_DEATH);
			$this->m_arrDeath[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrDeath[$i] = $res[$keyInfo];
			}
			
			// fkill
			$keyInfo = sprintf('%c%c', $weapon, STATS_FIRSTKILL);
			$this->m_arrFirstKill[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrFirstKill[$i] = $res[$keyInfo];
			}
			
			// fdeath
			$keyInfo = sprintf('%c%c', $weapon, STATS_FIRSTDEATH);
			$this->m_arrFirstDeath[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrFirstDeath[$i] = $res[$keyInfo];
			}
			
			// defu
			$keyInfo = sprintf('%c%c', $weapon, STATS_BOMB_DEFUSION);
			$this->m_arrDefu[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrDefu[$i] = $res[$keyInfo];
			}
			
			// plant
			$keyInfo = sprintf('%c%c', $weapon, STATS_BOMB_PLANTING);
			$this->m_arrPlant[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrPlant[$i] = $res[$keyInfo];
			}
			
			// 1k
			$keyInfo = sprintf('%c%c', $weapon, STATS_KILL_1);
			$this->m_arr1k[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr1k[$i] = $res[$keyInfo];
			}
			
			// 2k
			$keyInfo = sprintf('%c%c', $weapon, STATS_KILL_2);
			$this->m_arr2k[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr2k[$i] = $res[$keyInfo];
			}
			
			// 3k
			$keyInfo = sprintf('%c%c', $weapon, STATS_KILL_3);
			$this->m_arr3k[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr3k[$i] = $res[$keyInfo];
			}
			
			// 4k
			$keyInfo = sprintf('%c%c', $weapon, STATS_KILL_4);
			$this->m_arr4k[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr4k[$i] = $res[$keyInfo];
			}
			
			// 5k
			$keyInfo = sprintf('%c%c', $weapon, STATS_KILL_5);
			$this->m_arr5k[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr5k[$i] = $res[$keyInfo];
			}
			
			// 6k
			$keyInfo = sprintf('%c%c', $weapon, STATS_KILL_6);
			$this->m_arr6k[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr6k[$i] = $res[$keyInfo];
			}
			
			// 7k
			$keyInfo = sprintf('%c%c', $weapon, STATS_KILL_7);
			$this->m_arr7k[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr7k[$i] = $res[$keyInfo];
			}
			
			// 8k
			$keyInfo = sprintf('%c%c', $weapon, STATS_KILL_8);
			$this->m_arr8k[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr8k[$i] = $res[$keyInfo];
			}
			
			// 9k
			$keyInfo = sprintf('%c%c', $weapon, STATS_KILL_9);
			$this->m_arr9k[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr9k[$i] = $res[$keyInfo];
			}
			
			// 10k
			$keyInfo = sprintf('%c%c', $weapon, STATS_KILL_10);
			$this->m_arr10k[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr10k[$i] = $res[$keyInfo];
			}
			
			// 11k
			$keyInfo = sprintf('%c%c', $weapon, STATS_KILL_11);
			$this->m_arr11k[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr11k[$i] = $res[$keyInfo];
			}
			
			// 12k
			$keyInfo = sprintf('%c%c', $weapon, STATS_KILL_12);
			$this->m_arr12k[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr12k[$i] = $res[$keyInfo];
			}
			
			// 13k
			$keyInfo = sprintf('%c%c', $weapon, STATS_KILL_13);
			$this->m_arr13k[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr13k[$i] = $res[$keyInfo];
			}
			
			// 14k
			$keyInfo = sprintf('%c%c', $weapon, STATS_KILL_14);
			$this->m_arr14k[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr14k[$i] = $res[$keyInfo];
			}
			
			// 15k
			$keyInfo = sprintf('%c%c', $weapon, STATS_KILL_15);
			$this->m_arr15k[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr15k[$i] = $res[$keyInfo];
			}
			
			// 1v1
			$keyInfo = sprintf('%c%c', $weapon, STATS_1V1);
			$this->m_arr1v1[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr1v1[$i] = $res[$keyInfo];
			}
			
			// 1v2
			$keyInfo = sprintf('%c%c', $weapon, STATS_1V2);
			$this->m_arr1v2[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr1v2[$i] = $res[$keyInfo];
			}
			
			// 1v3
			$keyInfo = sprintf('%c%c', $weapon, STATS_1V3);
			$this->m_arr1v3[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr1v3[$i] = $res[$keyInfo];
			}
			
			// 1v4
			$keyInfo = sprintf('%c%c', $weapon, STATS_1V4);
			$this->m_arr1v4[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr1v4[$i] = $res[$keyInfo];
			}
			
			// 1v5
			$keyInfo = sprintf('%c%c', $weapon, STATS_1V5);
			$this->m_arr1v5[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr1v5[$i] = $res[$keyInfo];
			}
			
			// 1v6
			$keyInfo = sprintf('%c%c', $weapon, STATS_1V6);
			$this->m_arr1v6[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr1v6[$i] = $res[$keyInfo];
			}
			
			// 1v7
			$keyInfo = sprintf('%c%c', $weapon, STATS_1V7);
			$this->m_arr1v7[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr1v7[$i] = $res[$keyInfo];
			}
			
			// 1v8
			$keyInfo = sprintf('%c%c', $weapon, STATS_1V8);
			$this->m_arr1v8[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr1v8[$i] = $res[$keyInfo];
			}
			
			// 1v9
			$keyInfo = sprintf('%c%c', $weapon, STATS_1V9);
			$this->m_arr1v9[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr1v9[$i] = $res[$keyInfo];
			}
			
			// 1v10
			$keyInfo = sprintf('%c%c', $weapon, STATS_1V10);
			$this->m_arr1v10[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr1v10[$i] = $res[$keyInfo];
			}
			
			// 1v11
			$keyInfo = sprintf('%c%c', $weapon, STATS_1V11);
			$this->m_arr1v11[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr1v11[$i] = $res[$keyInfo];
			}
			
			// 1v12
			$keyInfo = sprintf('%c%c', $weapon, STATS_1V12);
			$this->m_arr1v12[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr1v12[$i] = $res[$keyInfo];
			}
			
			// 1v13
			$keyInfo = sprintf('%c%c', $weapon, STATS_1V13);
			$this->m_arr1v13[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr1v13[$i] = $res[$keyInfo];
			}
			
			// 1v14
			$keyInfo = sprintf('%c%c', $weapon, STATS_1V14);
			$this->m_arr1v14[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr1v14[$i] = $res[$keyInfo];
			}
			
			// 1v15
			$keyInfo = sprintf('%c%c', $weapon, STATS_1V15);
			$this->m_arr1v15[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arr1v15[$i] = $res[$keyInfo];
			}
			
			
			// 获取总回合数 STATS_ROUND
			$keyInfo = sprintf('%c%c', $weapon, STATS_ROUND);
			$RoundCnt = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$RoundCnt = $res[$keyInfo];
			}
			
			// 回合：T杀完胜利
			$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_T);
			$RndWinT = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$RndWinT = $res[$keyInfo];
			}
			
			// 回合：T爆炸胜利
			$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_BOOM);
			$RndWinBoom = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$RndWinBoom = $res[$keyInfo];
			}
			
			// 回合：CT杀完胜利
			$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_CT);
			$RndWinCT = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$RndWinCT = $res[$keyInfo];
			}
			
			// 回合：CT拆弹胜利
			$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_DEFUSE);
			$RndWinDefuse = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$RndWinDefuse = $res[$keyInfo];
			}

			// 回合：CT时间结束胜利
			$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_SAVED);
			$RndWinSaved = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$RndWinSaved = $res[$keyInfo];
			}
			
			// 回合：CT解救人质胜利
			$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_RESCUE);
			$RndWinRescue = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$RndWinRescue = $res[$keyInfo];
			}
			
			// 回合：CT未解救人质胜利
			$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_NOT_RESCUE);
			$RndWinNotRescue = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$RndWinNotRescue = $res[$keyInfo];
			}
			
			// 回合：保留胜利1
			$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_TYPE1);
			$RndWinType1 = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$RndWinType1 = $res[$keyInfo];
			}
			
			// 回合：保留胜利2
			$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_TYPE2);
			$RndWinType2 = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$RndWinType2 = $res[$keyInfo];
			}
			
			// 总胜利场次
			$RndWinTotal = $RndWinT + $RndWinBoom + $RndWinCT + $RndWinDefuse + $RndWinSaved + $RndWinRescue + $RndWinNotRescue + $RndWinType1 + $RndWinType2;
			
			// 失败回合
			$keyInfo = sprintf('%c%c', $weapon, STATS_RLOSE);
			$RndLose = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$RndLose = $res[$keyInfo];
			}
			
			// 平局回合
			$keyInfo = sprintf('%c%c', $weapon, STATS_REVEN);
			$RndEven = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$RndEven = $res[$keyInfo];
			}

			// 获取胜率
			if($RoundCnt != 0 )
				$this->m_arrWin[$i] = $RndWinTotal * 100 / $RoundCnt;
			else
				$this->m_arrWin[$i] = $RndWinTotal * 100 / 1.0;
			
			// 获取T胜率
			$RndWinTotalT = $RndWinT + $RndWinBoom + $RndWinNotRescue;
			if($RndWinTotal != 0 )
				$this->m_arrWinT[$i] = $RndWinTotalT * 100 / $RndWinTotal;
			else
				$this->m_arrWinT[$i] = 0;
			
			// 获取CT胜率
			$RndWinTotalCT = $RndWinCT + $RndWinDefuse + $RndWinSaved + $RndWinRescue;
			if($RndWinTotal != 0 )
				$this->m_arrWinCT[$i] = $RndWinTotalCT * 100 / $RndWinTotal;
			else
				$this->m_arrWinCT[$i] = 0;
			
			// 助攻
			$keyInfo = sprintf('%c%c', $weapon, STATS_ASSIST);
			$this->m_arrAssist[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrAssist[$i] = $res[$keyInfo];
			}
			
			// MVP
			$keyInfo = sprintf('%c%c', $weapon, STATS_MVP);
			$this->m_arrMvp[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrMvp[$i] = $res[$keyInfo];
			}
			
			// 获取ADR(胜方)
			$keyInfo = sprintf('%c%c', $weapon, STATS_ADR);
			$adrTotal = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$adrTotal = $res[$keyInfo];
			}
			if($RndWinTotal != 0 )
				$this->m_arrAdr[$i] = $adrTotal / $RndWinTotal;
			else
				$this->m_arrAdr[$i] = $adrTotal;

			// 获取RWS(双方)
			$keyInfo = sprintf('%c%c', $weapon, STATS_RWS);
			$rwsTotal = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$rwsTotal = $res[$keyInfo];
			}
			if($RoundCnt != 0 )
				$this->m_arrRWS[$i] = $rwsTotal / $RoundCnt;
			else
				$this->m_arrRWS[$i] = $rwsTotal;

			// online
			$keyInfo = sprintf('%c%c', $weapon, STATS_TIME_ONLINE);
			$this->m_arrOnline[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrOnline[$i] = $res[$keyInfo];
			}
			
			// kd
			if($this->m_arrDeath[$i] != 0 )
				$this->m_arrKD[$i] = sprintf("%0.2f", $this->m_arrKill[$i] / $this->m_arrDeath[$i]);
			else
				$this->m_arrKD[$i] = sprintf("%0.2f", $this->m_arrKill[$i] / 1.0);
			
			// kd head
			if($this->m_arrKill[$i] != 0)
				$this->m_arrHdKD[$i] = $this->m_arrHead[$i] * 100 / $this->m_arrKill[$i];
			else
				$this->m_arrHdKD[$i] = 0.00;
			
			// 命中率
			if($this->m_arrShot[$i] != 0 )
				$this->m_arrShotPeer[$i] = $this->m_arrHit[$i] * 100 / $this->m_arrShot[$i];
			else
				$this->m_arrShotPeer[$i] = $this->m_arrHit[$i] * 100 / 1.0;

			// Rating
			if($this->m_arrDeath[$i] == 0){
				$playerKD = ($this->m_arrKill[$i] / 1.0);
				if($playerKD > 8.0)
					$playerKD = 1.8;
				else if($playerKD >= 7.0 && $playerKD < 8.0)
					$playerKD = 1.6;
				else if($playerKD >= 6.0 && $playerKD < 7.0)
					$playerKD = 1.4;
				else if($playerKD >= 5.0 && $playerKD < 6.0)
					$playerKD = 1.2;
				else if($playerKD >= 4.0 && $playerKD < 5.0)
					$playerKD = 1.2;
				else if($playerKD >= 3.0 && $playerKD < 4.0)
					$playerKD = 1.1;
				else if($playerKD >= 2.0 && $playerKD < 3.0)
					$playerKD = 1.1;
				else if($playerKD >= 1.0 && $playerKD < 2.0)
					$playerKD = 1.0;
				
				$this->m_arrRating[$i] = $playerKD + 
										 $this->m_arrKill[$i] * 0.01 + 
										 $this->m_arrAssist[$i] * 0.02 +
										 $this->m_arrFirstKill[$i] * 0.01 -
										 $this->m_arrFirstDeath[$i] * 0.01 +
										 $this->m_arrPlant[$i] * 0.04 +
										 $this->m_arrDefu[$i] * 0.04 +
										 ($this->m_arr1k[$i] + $this->m_arr2k[$i] + $this->m_arr3k[$i] + $this->m_arr4k[$i] + $this->m_arr5k[$i] +
										  $this->m_arr6k[$i] + $this->m_arr7k[$i] + $this->m_arr8k[$i] + $this->m_arr9k[$i] + $this->m_arr10k[$i] +
										  $this->m_arr11k[$i] + $this->m_arr12k[$i] + $this->m_arr13k[$i] + $this->m_arr14k[$i] + $this->m_arr15k[$i]) * 0.02 +
										 ($this->m_arr1v1[$i] + $this->m_arr1v2[$i] + $this->m_arr1v3[$i] + $this->m_arr1v4[$i] + $this->m_arr1v5[$i] +
										  $this->m_arr1v6[$i] + $this->m_arr1v7[$i] + $this->m_arr1v8[$i] + $this->m_arr1v9[$i] + $this->m_arr1v10[$i] +
										  $this->m_arr1v11[$i] + $this->m_arr1v12[$i] + $this->m_arr1v13[$i] + $this->m_arr1v14[$i] + $this->m_arr1v15[$i]) * 0.01 +
										 $this->m_arrMvp[$i] * 0.01 +
										 $this->m_arrAdr[$i] * 0.002 +
										 $this->m_arrRWS[$i] * 0.001 +
										 $this->m_arrShotPeer[$i] * 0.003 +
										 $this->m_arrWin[$iPos] * 0.002;
			}
			else{
				$playerKD = ($this->m_arrKill[$i] / $this->m_arrDeath[$i]);
				if($playerKD > 8.0)
					$playerKD = 1.8;
				else if($playerKD >= 7.0 && $playerKD < 8.0)
					$playerKD = 1.6;
				else if($playerKD >= 6.0 && $playerKD < 7.0)
					$playerKD = 1.4;
				else if($playerKD >= 5.0 && $playerKD < 6.0)
					$playerKD = 1.2;
				else if($playerKD >= 4.0 && $playerKD < 5.0)
					$playerKD = 1.2;
				else if($playerKD >= 3.0 && $playerKD < 4.0)
					$playerKD = 1.1;
				else if($playerKD >= 2.0 && $playerKD < 3.0)
					$playerKD = 1.1;
				else if($playerKD >= 1.0 && $playerKD < 2.0)
					$playerKD = 1.0;
				
				$this->m_arrRating[$i] = $playerKD + 
										 $this->m_arrKill[$i] * 0.01 + 
										 $this->m_arrAssist[$i] * 0.02 +
										 $this->m_arrFirstKill[$i] * 0.01 -
										 $this->m_arrFirstDeath[$i] * 0.01 +
										 $this->m_arrPlant[$i] * 0.04 +
										 $this->m_arrDefu[$i] * 0.04 +
										 ($this->m_arr1k[$i] + $this->m_arr2k[$i] + $this->m_arr3k[$i] + $this->m_arr4k[$i] + $this->m_arr5k[$i] +
										  $this->m_arr6k[$i] + $this->m_arr7k[$i] + $this->m_arr8k[$i] + $this->m_arr9k[$i] + $this->m_arr10k[$i] +
										  $this->m_arr11k[$i] + $this->m_arr12k[$i] + $this->m_arr13k[$i] + $this->m_arr14k[$i] + $this->m_arr15k[$i]) * 0.02 +
										 ($this->m_arr1v1[$i] + $this->m_arr1v2[$i] + $this->m_arr1v3[$i] + $this->m_arr1v4[$i] + $this->m_arr1v5[$i] +
										  $this->m_arr1v6[$i] + $this->m_arr1v7[$i] + $this->m_arr1v8[$i] + $this->m_arr1v9[$i] + $this->m_arr1v10[$i] +
										  $this->m_arr1v11[$i] + $this->m_arr1v12[$i] + $this->m_arr1v13[$i] + $this->m_arr1v14[$i] + $this->m_arr1v15[$i]) * 0.01 +
										 $this->m_arrMvp[$i] * 0.01 +
										 $this->m_arrAdr[$i] * 0.002 +
										 $this->m_arrRWS[$i] * 0.001 +
										 $this->m_arrShotPeer[$i] * 0.003 +
										 $this->m_arrWin[$iPos] * 0.002;
				if($this->m_arrRating[$i] < 0.0)
					$this->m_arrRating[$i] = 0.0;
			}
			
			///////////////////////////////////////////////////////////
			// 穿墙命中次数
			$keyInfo = sprintf('%c%c', $weapon, STATS_WALL_HIT);
			$this->m_arrWallHit[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrWallHit[$i] = $res[$keyInfo];
			}
			
			//echo $this->m_arrWallHit[$i];
			//echo "</br>";
			
			// 穿墙射击伤害
			$keyInfo = sprintf('%c%c', $weapon, STATS_WALL_DAMAGE);
			$this->m_arrWallDamage[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrWallDamage[$i] = $res[$keyInfo];
			}
			
			// 穿墙爆头次数
			$keyInfo = sprintf('%c%c', $weapon, STATS_WALL_HEAD);
			$this->m_arrWallHead[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrWallHead[$i] = $res[$keyInfo];
			}
			
			// 穿墙击杀次数
			$keyInfo = sprintf('%c%c', $weapon, STATS_WALL_KILL);
			$this->m_arrWallKill[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrWallKill[$i] = $res[$keyInfo];
			}
			
			///////////////////////////////////////////////////////////
			// 被穿墙命中次数
			$keyInfo = sprintf('%c%c', $weapon, STATS_BWALL_HIT);
			$this->m_arrBWallHit[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrBWallHit[$i] = $res[$keyInfo];
			}
			
			// 被穿墙射击伤害
			$keyInfo = sprintf('%c%c', $weapon, STATS_BWALL_DAMAGE);
			$this->m_arrBWallDamage[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrBWallDamage[$i] = $res[$keyInfo];
			}
			
			// 被穿墙爆头次数
			$keyInfo = sprintf('%c%c', $weapon, STATS_BWALL_HEAD);
			$this->m_arrBWallHead[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrBWallHead[$i] = $res[$keyInfo];
			}
			
			// 被穿墙击杀次数
			$keyInfo = sprintf('%c%c', $weapon, STATS_BWALL_KILL);
			$this->m_arrBWallKill[$i] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_arrBWallKill[$i] = $res[$keyInfo];
			}
			
			// 当前等级、头衔
			$score = $this->m_arrScore[$i];
			$this->m_arrLevel[$i]  		= get_user_level($score);
			$this->m_arrMilitary[$i]  	= get_user_military($this->m_arrLevel[$i]);
		}
	}
	
	// 从REDIS读取排名和UID列表
	function loadRank()
	{
		// 读取总人数
		$this->getMaxPlayer();
		
		//获取全部积分(从大到小)
		$scoreKey = getKey($this->m_vType, $this->m_iDate);

		// 取出积分、id
		$score_uid = $this->m_pRedis->zRevRange($scoreKey, $this->m_nStart, $this->m_nEnd - 1, true);
		$this->m_arrUid  = array_keys($score_uid);

		$arrScore = array_values($score_uid);
		$arrLen = count($arrScore);

		for($i=0;$i<$arrLen;$i++)
		{
			$pos = stripos($arrScore[$i], '.');
			if(strlen($arrScore[$i]) >= ($pos + 2))
				$newscore = substr($arrScore[$i], 0, $pos + 2);
			else
				$newscore = $arrScore[$i];
				//$newscore = substr($arrScore[$i], 0, $pos);

			$this->m_arrScore[$i] = $newscore;
		}
		
		$this->loadBase();
		$this->loadUserInfo();
		//$this->load_all_user();
	}
}

?>