<?php

// http://www.amxmodx.org/api/cstrike_const

// 武器开始编号
const WEAPON_UNKNOW 		= 64;		// 未知武器 @
const WEAPON_MAX			= 32;		// 最大武器种类

const RANK_ALL				= 'all';	// 总榜
const RANK_YEAR				= 'year';	// 年榜
const RANK_MONTH			= 'month';	// 月榜
const RANK_DAY				= 'day';	// 日榜
const RANK_SESSION			= 's';		// 赛季榜，示例：s2023_1, s2023_2, s2023_3, s2023_4

// 玩家数据类型
const STATS_UNKNOW			= 64;						// 未知						+0
const STATS_KILL 			= STATS_UNKNOW + 1;			// 杀敌						+2
const STATS_SHOT 			= STATS_UNKNOW + 2;			// 射击						+0
const STATS_HEADSHOT		= STATS_UNKNOW + 3;			// 爆头						+1
const STATS_HIT				= STATS_UNKNOW + 4;			// 击中						+0
const STATS_DAMAGE			= STATS_UNKNOW + 5;			// 伤害						+0
const STATS_DEATH			= STATS_UNKNOW + 6;			// 死亡						-2
const STATS_FIRSTKILL		= STATS_UNKNOW + 7;			// 首杀						+1
const STATS_FIRSTDEATH		= STATS_UNKNOW + 8;			// 首死						+0
const STATS_BOMB_DEFUSION	= STATS_UNKNOW + 9;			// 拆除C4					+2
const STATS_BOMB_PLANTING	= STATS_UNKNOW + 10;		// 安装C4					+2
const STATS_TIME_ONLINE		= STATS_UNKNOW + 11;		// 在线						+0.02  每秒+0.002分(每小时7.2分)
const STATS_KILL_WORLD		= STATS_UNKNOW + 12;		// 摔死						-0.01
const STATS_KILL_SELF		= STATS_UNKNOW + 13;		// 自杀次数					-0.01
const STATS_MAX_PLAYER		= STATS_UNKNOW + 14;		// 最大玩家					+0
const STATS_RANK			= STATS_UNKNOW + 15;		// 当前排名					+0
const STATS_SCORE			= STATS_UNKNOW + 16;		// 当前积分					+0

// 武器击中身体部位伤害值
const STATS_DMAGE_NONE		= STATS_UNKNOW + 17;		// 击中空枪					+0
const STATS_DMAGE_HEAD		= STATS_UNKNOW + 18;		// 击中头部					+0
const STATS_DMAGE_CHEST		= STATS_UNKNOW + 19;		// 击中胸部					+0
const STATS_DMAGE_STOMACH	= STATS_UNKNOW + 20;		// 击中胃部					+0
const STATS_DMAGE_LEFTARM	= STATS_UNKNOW + 21;		// 击中左臂					+0
const STATS_DMAGE_RIGHTARM	= STATS_UNKNOW + 22;		// 击中右臂					+0
const STATS_DMAGE_LEFTEG	= STATS_UNKNOW + 23;		// 击中左脚					+0
const STATS_DMAGE_RIGHTEG	= STATS_UNKNOW + 24;		// 击中右脚					+0
const STATS_DMAGE_SHIELD	= STATS_UNKNOW + 25;		// 击中盾牌					+0

// 武器+BKILL
const STATS_BKILL			= STATS_UNKNOW + 26;		// 被击杀					+0
const STATS_BHEAD			= STATS_UNKNOW + 27;		// 被爆头					+0

// 武器击中身体部位次数 add by MT 2023-09-13
const STATS_HIT_NONE        = STATS_UNKNOW + 28;		// 击中空枪					+0
const STATS_HIT_HEAD        = STATS_UNKNOW + 29;        // 击中头部					+0
const STATS_HIT_CHEST       = STATS_UNKNOW + 30;        // 击中胸部					+0
const STATS_HIT_STOMACH     = STATS_UNKNOW + 31;        // 击中胃部					+0
const STATS_HIT_LEFTARM     = STATS_UNKNOW + 32;        // 击中左臂					+0
const STATS_HIT_RIGHTARM    = STATS_UNKNOW + 33;        // 击中右臂					+0
const STATS_HIT_LEFTEG      = STATS_UNKNOW + 34;        // 击中左脚					+0
const STATS_HIT_RIGHTEG     = STATS_UNKNOW + 35;        // 击中右脚					+0
const STATS_HIT_SHIELD      = STATS_UNKNOW + 36;        // 击中盾牌					+0

// 混战参数 add by MT 2023-09-30
const STATS_ROUND      		= STATS_UNKNOW + 37;		// 总回合 					+0
const STATS_RWIN_T			= STATS_UNKNOW + 38;		// 回合：T杀完胜利			+0.1
const STATS_RWIN_BOOM		= STATS_UNKNOW + 39;		// 回合：T爆炸胜利			+0.1
const STATS_RWIN_CT			= STATS_UNKNOW + 40;		// 回合：CT杀完胜利			+0.1
const STATS_RWIN_DEFUSE		= STATS_UNKNOW + 41;		// 回合：CT拆弹胜利			+0.1
const STATS_RWIN_SAVED		= STATS_UNKNOW + 42;		// 回合：CT时间结束胜利		+0.1
const STATS_RWIN_RESCUE		= STATS_UNKNOW + 43;		// 回合：CT解救人质胜利		+0.1
const STATS_RWIN_NOT_RESCUE	= STATS_UNKNOW + 44;		// 回合：CT未解救人质胜利	+0.1
const STATS_RWIN_TYPE1		= STATS_UNKNOW + 45;		// 回合：保留胜利1			+0
const STATS_RWIN_TYPE2		= STATS_UNKNOW + 46;		// 回合：保留胜利2			+0

const STATS_RLOSE			= STATS_UNKNOW + 47;		// 失败回合					-0.05
const STATS_REVEN			= STATS_UNKNOW + 48;		// 平局回合					+0.01

// 比赛参数
const STATS_SESSION			= STATS_UNKNOW + 49;		// 总场次					+0
const STATS_SWIN			= STATS_UNKNOW + 50;		// 胜利场次					+5
const STATS_SLOSE			= STATS_UNKNOW + 51;		// 失败场次					-3
const STATS_SEVEN			= STATS_UNKNOW + 52;		// 平局场次					+3
const STATS_MVP				= STATS_UNKNOW + 53;		// 最佳次数					+0.1
const STATS_RWS				= STATS_UNKNOW + 54;		// 每局贡献评分				+0

// 每回合杀敌统计
const STATS_KILL_0			= STATS_UNKNOW + 55;		// 0K						+0
const STATS_KILL_1			= STATS_UNKNOW + 56;		// 1K						+0.01
const STATS_KILL_2			= STATS_UNKNOW + 57;		// 2K						+0.02
const STATS_KILL_3			= STATS_UNKNOW + 58;		// 3K						+0.03
const STATS_KILL_4			= STATS_UNKNOW + 59;		// 4K						+0.04
const STATS_KILL_5			= STATS_UNKNOW + 60;		// 5K						+0.05
const STATS_KILL_6			= STATS_UNKNOW + 61;		// 6K						+0.06
const STATS_KILL_7			= STATS_UNKNOW + 62;		// 7K						+0.07
const STATS_KILL_8			= STATS_UNKNOW + 63;		// 8K						+0.08
const STATS_KILL_9			= STATS_UNKNOW + 64;		// 9K						+0.09
const STATS_KILL_10			= STATS_UNKNOW + 65;		// 10K						+0.10
const STATS_KILL_11			= STATS_UNKNOW + 66;		// 11K						+0.11
const STATS_KILL_12			= STATS_UNKNOW + 67;		// 12K						+0.12
const STATS_KILL_13			= STATS_UNKNOW + 68;		// 13K						+0.13
const STATS_KILL_14			= STATS_UNKNOW + 69;		// 14K						+0.14
const STATS_KILL_15			= STATS_UNKNOW + 70;		// 15K						+0.15
const STATS_KILL_16			= STATS_UNKNOW + 71;		// 16K						+0.16

// 残局胜利统计
const STATS_1V1				= STATS_UNKNOW + 72;		// 1v1						+0.02
const STATS_1V2				= STATS_UNKNOW + 73;		// 1v2						+0.03
const STATS_1V3				= STATS_UNKNOW + 74;		// 1v3						+0.04
const STATS_1V4				= STATS_UNKNOW + 75;		// 1v4						+0.05
const STATS_1V5				= STATS_UNKNOW + 76;		// 1v5						+1.0
const STATS_1V6				= STATS_UNKNOW + 77;		// 1v6						+1.2
const STATS_1V7				= STATS_UNKNOW + 78;		// 1v7						+1.4
const STATS_1V8				= STATS_UNKNOW + 79;		// 1v8						+1.6
const STATS_1V9				= STATS_UNKNOW + 80;		// 1v9						+1.8
const STATS_1V10			= STATS_UNKNOW + 81;		// 1v10						+2.0
const STATS_1V11			= STATS_UNKNOW + 82;		// 1v11						+2.2
const STATS_1V12			= STATS_UNKNOW + 83;		// 1v12						+2.4
const STATS_1V13			= STATS_UNKNOW + 84;		// 1v13						+2.6
const STATS_1V14			= STATS_UNKNOW + 85;		// 1v14						+2.8
const STATS_1V15			= STATS_UNKNOW + 86;		// 1v15						+3.0
const STATS_1V16			= STATS_UNKNOW + 87;		// 1v15						+3.5

const STATS_1ROUND			= STATS_UNKNOW + 88;		// 残局总数					+0
const STATS_1WIN			= STATS_UNKNOW + 89;		// 残局胜利					+0.1
const STATS_ASSIST			= STATS_UNKNOW + 90;		// 助攻						+0.1
const STATS_ADR				= STATS_UNKNOW + 91;		// ADR						+0

// 穿墙信息 add by MT 2023-10-03
const STATS_WALL_HIT		= STATS_UNKNOW + 92;		// 穿墙命中次数				+0.11
const STATS_WALL_DAMAGE		= STATS_UNKNOW + 93;		// 穿墙射击伤害				+0
const STATS_WALL_HEAD		= STATS_UNKNOW + 94;		// 穿墙爆头次数				+0.1
const STATS_WALL_KILL		= STATS_UNKNOW + 95;		// 穿墙击杀次数				+0.1

const STATS_BWALL_HIT		= STATS_UNKNOW + 96;		// 被穿墙命中次数			-0.01
const STATS_BWALL_DAMAGE	= STATS_UNKNOW + 97;		// 被穿墙射击伤害			+0
const STATS_BWALL_HEAD		= STATS_UNKNOW + 98;		// 被穿墙爆头次数			-0.01
const STATS_BWALL_KILL		= STATS_UNKNOW + 99;		// 被穿墙击杀次数			-0.01
	
global $g_arrChsName;		// 中文名
global $g_arrVipStr;		// VIP权限

// 获取中文名
function xgetChsName($iPos)
{
	global $g_arrChsName;
	if(count($g_arrChsName))
		return $g_arrChsName[$iPos];
	else
		return "VGuard";
}
	
// 获取VIP权限
function xgetVipStr($iPos)
{
	global $g_arrVipStr;
	if(count($g_arrVipStr))
		return $g_arrVipStr[$iPos];
	else
		return "普通会员";
}

// 判断日期是否大于当前日期 (检查VIP是否过期)
function dateBCurrent($date)
{ 
	//
	$currentDate=date("Y-m-d"); 
	
	//获取当前日期 
	$cYear=date("Y",strtotime($currentDate)); 
	$cMonth=date("m",strtotime($currentDate)); 
	$cDay=date("d",strtotime($currentDate)); 
	$year=date("Y",strtotime($date)); 
	$month=date("m",strtotime($date)); 
	$day=date("d",strtotime($date)); 
	$currentUnix=mktime(0,0,0,$cMonth,$cDay,$cYear); 

	//当前日期的 Unix 时间戳 
	$dateUnix=mktime(0,0,0,$month,$day,$year); 

	//待比较日期的 Unix 时间戳 
	if($dateUnix<=$currentUnix)
	{ 
		return true; 
	}else{ 
		return false; 
	} 
}

// 获取权限字母
// $Access:		abcdef
// $ExpDate:	2022-05-20
function get_access_str($sAccess, $ExpDate)
{
	$vip1   = 'l';
	$vip2   = 'm';
	$vip3   = 'n';
	$vip4   = 'p';
	
	$fstr = '';
	$result = '';
	$flag1 = '超级管理员';
	$flag2 = '管理员';
	$flag3 = '超级VIP';
	$flag4 = 'VIP';
	$flag5 = '免费会员';
	
	if(strpos($sAccess, $vip1) != false)
		$fstr = $flag1;
	else if(strpos($sAccess, $vip2) != false)
		$fstr = $flag2;
	else if(strpos($sAccess, $vip3) != false)
		$fstr = $flag3;
	else if(strpos($sAccess, $vip4) != false)
		$fstr = $flag4;
	else 
		$fstr = $flag5;
	
	if(strlen($ExpDate))
	{
		if(!dateBCurrent($ExpDate))
			$result = sprintf("%s [%s]", $fstr, $ExpDate);
		else
			$result = $flag5;
	}
	else
	{
		if($fstr == $flag5)
			$result = $flag5;
		else
			$result = sprintf("%s [永久]", $fstr);
	}
	
	return $result;
}


// 获取当前KEY
function getKey($vType, $iDate)
{
	$skey = "a";
	switch($vType)
	{
	case RANK_ALL:		// a			总榜
		$skey = "a";
		break;
		
	case RANK_YEAR:		// y2022		年傍
		//$skey = sprintf("y%s", date("Y"));
		$skey = sprintf("y%s", $iDate);
		break;
		
	case RANK_MONTH:	// m202205		月榜
		$skey = sprintf("m%s", $iDate);
		break;
	
	case RANK_DAY:		// d20220501	日榜
		$skey = sprintf("d%s", $iDate);
		break;
		
	default:
		break;
	}	
	return $skey;
}

?>
