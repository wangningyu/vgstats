<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link href="static/web.css" rel="stylesheet" type="text/css"/>
	<title>【VGStats】 - 总排行榜</title>
</head>
<body>

<?php
	require_once ("Military.php");
	require_once ("MTMySql.php");
	require_once ("MTRedisServer.php");
	
	require_once ("CSPub.php");
	require_once ("Military.php");
	require_once ("MTGlobalCFG.php");
	require_once ("MTPublic.php");
	require_once ("Military.php");
	require_once ("MTRedisServer.php");
	require_once ("MTRedisPlayer.php");

	//ini_set("display_errors", 0);
	//error_reporting(E_ALL ^ E_NOTICE);
	//error_reporting(E_ALL ^ E_WARNING);
	
	// 统计时间
	$start = microtime(true);
	
	// 开始连接redis
	$pRedis = new Redis();
	if (!$pRedis->connect($redis_host, $redis_port))
	{
		trigger_error('Redis连接出错.', E_USER_ERROR);
	}
	else
	{
		//echo 'Redis服务器连接OK.<br>';
		if(!$pRedis->auth($redis_pass))
		{
			trigger_error('Redis密码出错！！！', E_USER_ERROR);
		}
		else{
			//echo 'Redis密码验证OK.<br>';
		}
	}

	// 显示全部错误或警告
	//error_reporting(E_ALL);
	function get_client_name($username)
	{
		//$username = "VGuard-";
		$username = str_replace("&","&amp;",$username);
		$username = str_replace("<","&lt;",$username);
		$username = str_replace(">","&gt;",$username);
		return $username;
	}

	$showPage = 20;
	$showPage = $_GET["iShow"]; 		//获得当前的页面值
	if (!isset($showPage))
		$showPage = 20;
	if($showPage >= 20)
		$showPage = 20;
	
	$perCount = 100; 					//每页显示的记录数
	$perCount = $_GET['iCount']; 		//获得当前的页面值
	if (!isset($perCount))
		$perCount = 50;
	if($perCount >= 1000)
		$perCount = 1000;
	
	$page = $_GET['iPage']; 			//获得当前的页面值
	if (!isset($page)) {
		$page = 1;
	}

	$iType = $_GET['iType']; 			//获得显示类型
	if (!isset($iType)) {
		$iType = RANK_ALL;
	}
	
	// 如果不是显示总榜,则需要读取YYYY、YYYYMM、YYYYMMDD
	$iDate = '';
	if($iType == RANK_YEAR)
	{
		$iDate = $_GET['iDate']; 		//获得当前日期
		if (!isset($iType)) 
			$iDate = sprintf("y%s", date("Y"));
	}else if($iType == RANK_MONTH)
	{
		$iDate = $_GET['iDate']; 		//获得当前日期
		if (!isset($iDate)) 
			$iDate = sprintf("m%s", date("Ym"));
	}else if($iType == RANK_DAY)
	{
		$iDate = $_GET['iDate']; 		//获得当前日期
		if (!isset($iDate)) 
			$iDate = sprintf("m%s", date("Ymd"));
	}
	
	$pRank = new CRankHeader($pRedis, $iType, $iDate, $page, $perCount);
	$pRank->loadRank();

	$maxCnt = $pRank->getMaxPlayer();	//总玩家数量
	$totalPage = $maxCnt / $perCount;	//计算总页数
	if(($maxCnt % $perCount) != 0)
		$totalPage++;
	if($showPage >= $totalPage)
		$showPage = $totalPage;
	
	$startCount = ($page - 1) * $perCount; //分页开始,根据此方法计算出开始的记录
	$iRank = $startCount + 1;

	if(preg_match('/^[0-9]$/',$page))
	{
		//die ('排名页数输入有误，请不要尝试非法操作！');
	}

	echo "<h4 style=\"color:red;text-align:center;margin:12px 0 0 0;\">【VGStats】 - 总排行</h4>";
	echo "<div style=\"width:100%;margin:5px 0px 0px 0px;\" align=\"center\">";
	echo "<table class=weapons>
			<tr class=weapon_fields>
				<td>总排名</td>
				<td>名称</td>
				<td>权限</td>
				<td>杀敌</td>
				<td>助攻</td>
				<td>阵亡</td>
				<td>K/D</td>
				<td>爆头率</td>
				<td>命中率</td>
				<td>首杀</td>
				<td>首亡</td>
				<td>安C4</td>
				<td>拆C4</td>
				
				<td>穿墙</td>
				<td>穿爆头</td>
				<td>穿击杀</td>
				
				<td>1k</td>
				<td>2k</td>
				<td>3k</td>
				<td>1v1</td>
				<td>1v2</td>
				<td>1v3</td>
				<td>MVP</td>
				
				<td>总胜率</td>
				<td>T胜率</td>
				<td>CT胜率</td>
				
				<td>ADR</td>
				<td>RWS</td>
				<td>Rating</td>
				
				<td>在线</td>
				<td>积分</td>
				<td>头衔</td>
			</tr>";

	// 统计时间
	//$startSQL = microtime(true);
	//xload_all_user($pRank->m_arrUid);
	//$elapsedSQL = microtime(true) - $startSQL; 
	//echo "<br>读取SQL耗时: ";
	//echo $elapsedSQL;
	//echo " 秒<br>";

	for($iPos = 0; $iPos<count($pRank->m_arrUid); $iPos++)
	{
		if($iRank > $pRank->getRankEnd())
			break;
		
		$uid = $pRank->get_uid($iPos);
		
		//$chsname = $pRank->getChsName($iPos);
		//$chsname = get_client_name($chsname);
		//$vipstr = $pRank->getVipStr($iPos);
		
		//$chsname = xgetChsName($iPos);
		//$chsname = get_client_name($chsname);
		$chsname = $pRank->getChsName($iPos);
		$chsname = get_client_name($chsname);
		$vipstr = $pRank->getVipStr($iPos);
		//$vipstr = xgetVipStr($iPos);
		
		$kill = $pRank->get_kill($iPos);
		$death = $pRank->get_death($iPos);

		$szHeadKD = $pRank->getHdKD($iPos);
		$szKD = $pRank->getKD($iPos);
		$szHitKD = $pRank->getShotKD($iPos);
		
		$firstKill = $pRank->get_firstKill($iPos);
		$firstDeath = $pRank->get_firstDeath($iPos);
		
		$plant = $pRank->get_plant($iPos);
		$defu = $pRank->get_defu($iPos);
		$onlineTime = $pRank->get_online($iPos);
		$onlinestr = sprintf("%0.1fh", $onlineTime / 3600);
		
		$score = $pRank->getScore($iPos);
		$rankUrl   = sprintf("<a target=\"_blank\" href=\"Rank.php?uid=%d&iType=%s&iDate=%s\">", $uid, $iType, $iDate);
		$level = $pRank->getLevel($iPos);
		$military  = $pRank->getTitle($iPos);
		
		$wallHit = $pRank->get_wall_hit($iPos);
		$wallHead = $pRank->get_wall_head($iPos);
		$wallKill = $pRank->get_wall_kill($iPos);
		
		$k1 = $pRank->get_1k($iPos);
		$k2 = $pRank->get_2k($iPos);
		$k3 = $pRank->get_3k($iPos);
		//$k4 = $pRank->get_4k($iPos);
		//$k5 = $pRank->get_5k($iPos);
		$v1 = $pRank->get_1v1($iPos);
		$v2 = $pRank->get_1v2($iPos);
		$v3 = $pRank->get_1v3($iPos);
		//$v4 = $pRank->get_1v4($iPos);
		//$v5 = $pRank->get_1v5($iPos);
		$assist = $pRank->get_assist($iPos);
		$mvp = $pRank->get_mvp($iPos);
		
		$win = $pRank->get_win($iPos);
		$winT = $pRank->get_winT($iPos);
		$winCT = $pRank->get_winCT($iPos);
		
		$adr = $pRank->get_adr($iPos);
		$rws = $pRank->get_rws($iPos);
		$rating = $pRank->get_rating($iPos);
		
		$strEmpty = "-";
		
		echo "<tr class=data>
				<td class=color1>$rankUrl$iRank</a></td>
				<td class=color2>$rankUrl$chsname</a></td>
				<td class=color2>$rankUrl$vipstr</a></td>
				<td class=color1>$rankUrl$kill</a></td>
				<td class=color1>$rankUrl$assist</a></td>
				<td class=color2>$rankUrl$death</a></td>
				<td class=color1>$rankUrl$szKD</a></td>
				<td class=color2>$rankUrl$szHeadKD%</a></td>
				<td class=color2>$rankUrl$szHitKD%</a></td>
				<td class=color1>$rankUrl$firstKill</a></td>
				<td class=color2>$rankUrl$firstDeath</a></td>
				<td class=color1>$rankUrl$plant</a></td>
				<td class=color2>$rankUrl$defu</a></td>
				
				<td class=color2>$rankUrl$wallHit</a></td>
				<td class=color2>$rankUrl$wallHead</a></td>
				<td class=color2>$rankUrl$wallKill</a></td>
				
				<td class=color2>$rankUrl$k1</a></td>
				<td class=color2>$rankUrl$k2</a></td>
				<td class=color2>$rankUrl$k3</a></td>
				<td class=color2>$rankUrl$v1</a></td>
				<td class=color2>$rankUrl$v2</a></td>
				<td class=color2>$rankUrl$v3</a></td>
				<td class=color2>$rankUrl$mvp</a></td>
				
				<td class=color2>$rankUrl$win</a></td>
				<td class=color2>$rankUrl$winT</a></td>
				<td class=color2>$rankUrl$winCT</a></td>
				
				<td class=color2>$rankUrl$adr</a></td>
				<td class=color2>$rankUrl$rws</a></td>
				<td class=color2>$rankUrl$rating</a></td>
				
				<td class=color1>$rankUrl$onlinestr</a></td>
				<td class=color2>$rankUrl$score</a></td>
				<td class=color2>$rankUrl$military</a></td>
			</tr>";

		$iRank++;
	}
	
	echo "</table>";
	echo "</center>";
	echo "</table>";

	if($maxCnt <= 0)
	{
		$elapsed = microtime(true) - $start; 
		echo "<br>页面生成耗时: ";
		echo $elapsed;
		echo " 秒<br>";
		return;
	}

	// 显示首页
	$urlTemp = sprintf("<a target=\"_blank\" href=\"index.php?iType=%s&iDate=%s&iPage=1&iShow=%d&iCount=%d\">首页</a>&ensp;", $iType, $iDate, $showPage, $perCount);
	echo "$urlTemp";
	
	// 是否显示上一页
	if($page > 1)
		$urlTemp = sprintf("<a target=\"_blank\" href=\"index.php?iType=%s&iDate=%s&iPage=%d&iShow=%d&iCount=%d\">", $iType, $iDate, $page - 1, $showPage, $perCount);
	else
		$urlTemp = sprintf("<a target=\"_blank\" href=\"index.php?iType=%s&iDate=%s&iPage=%d&iShow=%d&iCount=%d\">", $iType, $iDate, $page, $showPage, $perCount);
	echo "$urlTemp 上一页</a>&ensp;";
	
	// 显示当前的前面10页
	if($page <= $showPage)
	{
		$minPage = 1;
		$maxPage = $showPage;
	}
	else{
		$minPage = $page - $showPage;
		$maxPage = $page;
	}
	
	// 显示当前的后面10页
	for ($iPage = $minPage; $iPage <= $maxPage ; $iPage++)
	{
		$curPage = $iPage;
		$urlTemp = sprintf("<a target=\"_blank\" href=\"index.php?iType=%s&iDate=%s&iPage=%d&iShow=%d&iCount=%d\">", $iType, $iDate, $curPage, $showPage, $perCount);
		echo "$urlTemp $curPage</a>&ensp;";
	}
	
	// 显示下一页
	$urlTemp = sprintf("<a target=\"_blank\" href=\"index.php?iType=%s&iDate=%s&iPage=%d&iShow=%d&iCount=%d\">", $iType, $iDate, $page + 1, $showPage, $perCount);
	echo "$urlTemp 下一页</a>&ensp;";

	// 显示最后一页
	if($totalPage > 1)
	{
		$urlTemp = sprintf("<a target=\"_blank\" href=\"index.php?iType=%s&iDate=%s&iPage=%d&iShow=%d&iCount=%d\">", $iType, $iDate, $totalPage, $showPage, $perCount);
		echo "$urlTemp 末页</a><br>";
	}

	// 显示总榜
	$urlTemp = sprintf("<a target=\"_blank\" href=\"index.php?iType=all\">总排行榜</a>&ensp;");
	echo "$urlTemp";
	
	// 显示年榜
	$urlTemp = sprintf("<a target=\"_blank\" href=\"index.php?iType=year&iDate=%s\">%s</a>&ensp;", date("Y"), date("Y"));
	echo "$urlTemp";
	
	// 显示月榜
	$urlTemp = sprintf("<a target=\"_blank\" href=\"index.php?iType=month&iDate=%s\">%s-%s</a>&ensp;", date("Ym"), date("Y"), date("m"));
	echo "$urlTemp";
	
	// 显示日榜
	$urlTemp = sprintf("<a target=\"_blank\" href=\"index.php?iType=day&iDate=%s\">%s-%s-%s</a>&ensp;", date("Ymd"), date("Y"), date("m"), date("d"));
	echo "$urlTemp";
	
	// 免费软件请保留友情链接,给个Star支持！
	// https://gitee.com/wangningyu/vgstats
	$elapsed = microtime(true) - $start; 
	$msc = sprintf("%0.8f", $elapsed);
	echo "<br><a target=\"_blank\" href=\"http://cs27015.com\" target=\"_blank\">Powered By VGStats 3.0</a>&ensp;页面生成耗时: ";
	echo $msc;
	echo " 秒<br>";

    echo "</center></div></h4>";
?>

</body> 
</html> 
