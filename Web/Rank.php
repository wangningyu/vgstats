<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link href="static/web.css" rel="stylesheet" type="text/css"/>
	<title>【VGStats】 - 玩家信息</title>
</head>
<body>

<?php
	require_once ("CSPub.php");
	require_once ("Military.php");
	require_once ("MTGlobalCFG.php");
	require_once ("MTPublic.php");
	require_once ("Military.php");
	require_once ("MTRedisServer.php");
	require_once ("MTRedisPlayer.php");

	//ini_set("display_errors", 0);
	//error_reporting(E_ALL ^ E_NOTICE);
	error_reporting(E_ALL ^ E_WARNING);
	//ini_set("display_errors", 0);
	//error_reporting(E_ALL ^ E_NOTICE);
	//error_reporting(E_ALL ^ E_WARNING);

	// 统计时间
	$start = microtime(true);

	$uid = $_GET['uid']; //获得当前玩家ID
	$myindex = $uid;
	if($myindex <= 0)
	{
		//die ("玩家数字编号输入有误，请不要尝试非法操作！");
	}
	
	$pRedis = new Redis();
	if (!$pRedis->connect($redis_host, $redis_port))
	{
		trigger_error('Redis连接出错.', E_USER_ERROR);
	}
	else
	{
		//echo 'Redis服务器连接OK.<br>';
		if(!$pRedis->auth($redis_pass))
		{
			trigger_error('Redis密码出错！！！', E_USER_ERROR);
		}
		else{
			//echo 'Redis密码验证OK.<br>';
		}
	}
	
	// 检查查询类型和日期范围
	$iType = $_GET['iType']; 			//获得显示类型
	if (!isset($iType) || strlen($iType) == 0) {
		$iType = RANK_ALL;
	}
	
	// 如果不是显示总榜,则需要读取YYYY、YYYYMM、YYYYMMDD
	$iDate = '';
	if($iType == RANK_YEAR)
	{
		$iDate = $_GET['iDate']; 		//获得当前日期
		if (!isset($iDate)) 
			$iDate = sprintf("y%s", date("Y"));
	}else if($iType == RANK_MONTH)
	{
		$iDate = $_GET['iDate']; 		//获得当前日期
		if (!isset($iDate)) 
			$iDate = sprintf("m%s", date("Ym"));
	}else if($iType == RANK_DAY)
	{
		$iDate = $_GET['iDate']; 		//获得当前日期
		if (!isset($iDate)) 
			$iDate = sprintf("m%s", date("Ymd"));
	}
	
	$player = new CPlayerInfo($pRedis, $iType, $iDate, $myindex);

?>

	<div class=main align="center">
		<div class=left>
			<div class=sub_left>
				<table class=info>
					<tr class=info_title>
						<td colspan="2">个人资料</td>
					</tr>
					<tr>
						<td class=info_name>英文名</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_name_en();?></span></td>
					</tr>
					<tr>
						<td class=info_name>中文名</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_name_ch();?></span></td>
					</tr>
					<tr>
						<td class=info_name>玩家权限</td><td class=info_value colspan="2"><span style="color:#ffff00;"><strong><?php echo $player->get_vip_show();?></strong></span><br /></td>
					</tr>
					<tr>
						<td class=info_name>注册日期</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_date_reg();?></span></td>
					</tr>
					<tr>
						<td class=info_name>上次登录</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_date_login();?></span></td>
					</tr>
					<tr>
						<td class=info_name>在线时长</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_time_online()?></span></td>
					</tr>
					<tr>
						<td class=info_name>玩家QQ</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_qq()?></span></td>
					</tr>
					<tr>
						<td class=info_name>个性签名</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_signature()?></span></td>
					</tr>
					<tr>
						<td class=info_name>玩家位置</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_location()?></span></td>
					</tr>

					<tr class=info_title>
						<td colspan="3">游戏数据</td>
					</tr>
					<tr>
						<td class=info_name>当前排名</td><td class=info_value colspan="2"><span style="color:#ffff00;"><strong><?php echo $player->get_rank()?></strong></span></td>
					</tr>
					<tr>
						<td class=info_name>玩家总数</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_max_player()?></span></td>
					</tr>
					<tr>
						<td class=info_name>游戏经验</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_score()?></span></td>
					</tr>
					<tr>
						<td class=info_name>游戏军衔</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_military()?></span></td>
					</tr>
					<tr>
						<td class=info_name>最先杀敌</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_first_kill()?></span></td>
					</tr>
					<tr>
						<td class=info_name>最先阵亡</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_first_death()?></span></td>
					</tr>
					<tr>
						<td class=info_name>拆除C4</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_first_defu()?></span></td>
					</tr>
					<tr>
						<td class=info_name>放置C4</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_first_plant()?></span></td>
					</tr>
					
					<tr>
						<td class=info_name>助攻</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_assist()?></span></td>
					</tr>
					<tr>
						<td class=info_name>MVP</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_mvp()?></span></td>
					</tr>
					<tr>
						<td class=info_name>T胜率</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_rnd_winT()?></span></td>
					</tr>
					<tr>
						<td class=info_name>CT胜率</td><td class=info_value colspan="2"><span style="color:#11ee11;"><?php echo $player->get_rnd_winCT()?></span></td>
					</tr>
				</table>
				
			</div>
		</div>
		<div class=right>
			<div class=sub_right>
				<table class=infob>
					<tr class=info_title>
						<td colspan="3">综合数据</td>
					</tr>
					<tr style="height:50px">
						<td class=info_nameb>杀敌</td> 
						<td class=bar rowspan="2">
							<div class="pbar">
								<div style="width:<?php echo $player->get_kd()?>;"><span>K/D: <?php echo $player->get_kd()?></span></div>
							</div>
						</td> 
						<td class=info_nameb>阵亡</td> 
					</tr> 
					<tr style="height:50px"> 
						<td class=info_valueb><span style="color:#11ee11;"><?php echo $player->get_all_kill()?></span></td> 
						<td class=info_valueb><span style="color:#11ee11;"><?php echo $player->get_all_death()?></span></td> 
					</tr> 
					
					<tr style="height:50px"> 
						<td class=info_nameb>爆头</td> 
						<td class=bar rowspan="2">
							<div class="pbar">
								<div style="width: <?php echo $player->get_head_kd()?>;"><span>爆头率: <?php echo $player->get_head_kd()?>%</span></div>
							</div>
						</td> 
						<td class=info_nameb>杀敌</td> 
					</tr> 
					<tr style="height:50px"> 
						<td class=info_valueb><span style="color:#11ee11;"><?php echo $player->get_all_head()?></span></td> 
						<td class=info_valueb><span style="color:#11ee11;"><?php echo $player->get_all_kill()?></span></td> 
					</tr> 
					
					<tr style="height:50px"> 
						<td class=info_nameb>命中</td> 
						<td class=bar rowspan="2">
							<div class="pbar">
								<div style="width: <?php echo $player->get_shot_kd()?>;"><span>命中率: <?php echo $player->get_shot_kd()?>%</span></div>
							</div>
						</td> 
						<td class=info_nameb>射击</td> 
					</tr> 
					<tr style="height:50px"> 
						<td class=info_valueb><span style="color:#11ee11;"><?php echo $player->get_all_hit()?></span></td> 
						<td class=info_valueb><span style="color:#11ee11;"><?php echo $player->get_all_shot()?></span></td> 
					</tr> 
					
					<tr style="height:50px"> 
						<td class=info_nameb>自雷</td> 
						<td class=bar rowspan="2">
							<div class="pbar">
								<div style="width: <?php echo $player->get_kws()?>;"><span>死亡率: <?php echo $player->get_kws()?>%</span></div>
							</div>
						</td> 
						<td class=info_nameb>摔死</td> 
					</tr> 
					<tr style="height:50px"> 
						<td class=info_valueb><span style="color:#11ee11;"><?php echo $player->get_kill_world()?></span></td> 
						<td class=info_valueb><span style="color:#11ee11;"><?php echo $player->get_kill_self()?></span></td> 
					</tr>
					
					<tr style="height:50px"> 
						<td class=info_nameb>胜利场</td> 
						<td class=bar rowspan="2">
							<div class="pbar">
								<div style="width: <?php echo $player->get_rnd_win()?>;"><span>总胜率: <?php echo $player->get_rnd_win()?></span></div>
							</div>
						</td> 
						<td class=info_nameb>总场次</td> 
					</tr> 
					<tr style="height:50px"> 
						<td class=info_valueb><span style="color:#11ee11;"><?php echo $player->get_rnd_win_cnt()?></span></td> 
						<td class=info_valueb><span style="color:#11ee11;"><?php echo $player->get_rnd_cnt()?></span></td> 
					</tr>
				</table>
			</div>
		</div>
		
		<div class=back>
				<table class=weapons>
					<tr class=info_title>
						<td colspan="19">伤害数据</td>
					</tr>
					<tr class=weapon_fields>
						<td style="width:10%">武器</td>
						<td style="width:5%">杀敌</td>
						<td style="width:5%">被杀</td>
						<td style="width:5%">射击</td>
						<td style="width:5%">命中</td>
						<td style="width:5%">被爆头</td>
						<td style="width:5%">爆头</td>						
						
						<td style="width:5%">总伤害</td>
						<td style="width:5%">头部</td>
						<td style="width:5%">胸部</td>
						<td style="width:5%">腹部</td>
						<td style="width:5%">左臂</td>
						<td style="width:5%">右臂</td>
						<td style="width:5%">左腿</td>
						<td style="width:5%">右腿</td>
						<td style="width:5%">盾牌</td>
						
						<td style="width:5%">爆头率</td>
						<td style="width:5%">被爆率</td>
						<td style="width:5%">命中率</td>
					</tr>
					<?php
					$StrSubDefault = "-"; // 设置一个默认值
					for($i=0; $i<$player->m_WpSize; $i++)
					{
						echo "<tr class=data>";
						echo "<td>";echo $player->m_arrWImage[$i]; echo"</td>";
						
						if($player->m_arrWKill[$i] == 0)
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWKill[$i]."</td>";
						
						if($player->m_arrWBKill[$i] == 0)
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWBKill[$i]."</td>";
						
						if($player->m_arrWShot[$i] == 0)
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWShot[$i]."</td>";

						if($player->m_arrWHit[$i] == 0)
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWHit[$i]."</td>";
						
						if($player->m_arrWBHead[$i] == 0)
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWBHead[$i]."</td>";

						if($player->m_arrHead[$i] == 0)
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrHead[$i]."</td>";

						if($player->m_arrWDamage[$i] == 0)
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWDamage[$i]."</td>";
						
						if($player->m_arrWHead[$i] == "0.0%")
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWHead[$i]."</td>";
						
						if($player->m_arrWChest[$i] == "0.0%")
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWChest[$i]."</td>";
						
						if($player->m_arrWBelly[$i] == "0.0%")
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWBelly[$i]."</td>";
						
						if($player->m_arrWArmLeft[$i] == "0.0%")
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWArmLeft[$i]."</td>";
						
						if($player->m_arrWArmRight[$i] == "0.0%")
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWArmRight[$i]."</td>";
						
						if($player->m_arrWLegLeft[$i] == "0.0%")
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWLegLeft[$i]."</td>";
						
						if($player->m_arrWLegRight[$i] == "0.0%")
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWLegRight[$i]."</td>";
						
						if($player->m_arrWShield[$i] == "0.0%")
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWShield[$i]."</td>";
						
						if($player->m_arrHeadKD[$i] == "0.0%")
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrHeadKD[$i]."</td>";
						
						if($player->m_arrWBHeadKD[$i] == "0.0%")
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWBHeadKD[$i]."</td>";
						
						if($player->m_arrShotKD[$i] == "0.0%")
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrShotKD[$i]."</td></br>";
					}
					?>
			</table>
		</div>
		
		<div class=back>
				<table class=weapons>
					<tr class=info_title>
						<td colspan="17">回合杀敌</td>
					</tr>
					<tr class=weapon_fields>
						<td style="width:5.8%">酱油局</td>
						<td style="width:5.8%">1K</td>
						<td style="width:5.8%">2K</td>
						<td style="width:5.8%">3K</td>
						<td style="width:5.8%">4K</td>
						<td style="width:5.8%">5K</td>
						<td style="width:5.8%">6K</td>
						<td style="width:5.8%">7K</td>
						<td style="width:5.8%">8K</td>
						<td style="width:5.8%">9K</td>
						<td style="width:5.8%">10K</td>
						<td style="width:5.8%">11K</td>
						<td style="width:5.8%">12K</td>
						<td style="width:5.8%">13K</td>
						<td style="width:5.8%">14K</td>
						<td style="width:5.8%">15K</td>
						<td style="width:5.8%">16K</td>
					</tr>
					<?php
				        echo "<tr class=data>";
						$StrSubDefault = "-"; // 设置一个默认值
						for($i=0; $i<=16; $i++)
						{
							if($player->m_RoundKill[$i] == 0)
								echo "<td>".$StrSubDefault."</td>";
							else
								echo "<td>".$player->m_RoundKill[$i]."</td>";
						}
				        echo "</tr>";
					?>
				</table>
			
				<table class=weapons>
					<tr class=info_title>
						<td colspan="17">残局杀敌</td>
					</tr>
					<tr class=weapon_fields>
						<td style="width:5.88%">残局胜率</td>
						<td style="width:5.88%">1v1</td>
						<td style="width:5.88%">1v2</td>
						<td style="width:5.88%">1v3</td>
						<td style="width:5.88%">1v4</td>
						<td style="width:5.88%">1v5</td>
						<td style="width:5.88%">1v6</td>
						<td style="width:5.88%">1v7</td>
						<td style="width:5.88%">1v8</td>
						<td style="width:5.88%">1v9</td>
						<td style="width:5.88%">1v10</td>
						<td style="width:5.88%">1v11</td>
						<td style="width:5.88%">1v12</td>
						<td style="width:5.88%">1v13</td>
						<td style="width:5.88%">1v14</td>
						<td style="width:5.88%">1v15</td>
						<td style="width:5.88%">1v16</td>
					</tr>
					<?php
				        echo "<tr class=data>";
						echo "<td>";echo $player->m_RemainPeer; echo"</td>";
						$StrSubDefault = "-"; // 设置一个默认值
						for($i=0; $i<16; $i++)
						{
							if($player->m_RoundRemain[$i] == 0)
								echo "<td>".$StrSubDefault."</td>";
							else
								echo "<td>".$player->m_RoundRemain[$i]."</td>";
						}
				        echo "</tr>";
					?>
				</table>
				
				<table class=weapons>
					<tr class=info_title>
						<td colspan="17">命中数据</td>
					</tr>
					<tr class=weapon_fields>
						<td style="width:10%">武器</td>
						<td style="width:10%">总击中</td>
						<td style="width:10%">头部</td>
						<td style="width:10%">胸部</td>
						<td style="width:10%">腹部</td>
						<td style="width:10%">左臂</td>
						<td style="width:10%">右臂</td>
						<td style="width:10%">左腿</td>
						<td style="width:10%">右腿</td>
						<td style="width:10%">盾牌</td>
					</tr>
					<?php
					for($i=0; $i<$player->m_WpSize; $i++)
					{
						if($player->m_arrShotKD[$i] == "0.0%")
							continue;
						
				        echo "<tr class=data>";
						echo "<td>";echo $player->m_arrWImage[$i]; echo"</td>";
						echo "<td>";echo $player->m_arrShotKD[$i]; echo"</td>";
						echo "<td>";echo $player->m_arrHitHead[$i]; echo"</td>";
						echo "<td>";echo $player->m_arrHitChest[$i]; echo"</td>";
						echo "<td>";echo $player->m_arrHitBelly[$i]; echo"</td>";
						echo "<td>";echo $player->m_arrHitArmLeft[$i]; echo"</td>";
						echo "<td>";echo $player->m_arrHitArmRight[$i]; echo"</td>";
						echo "<td>";echo $player->m_arrHitLegLeft[$i]; echo"</td>";
						echo "<td>";echo $player->m_arrHitLegRight[$i]; echo"</td>";
						echo "<td>";echo $player->m_arrHitShield[$i]; echo"</td>";
				        echo "</tr>";
					}
					?>
			</table>
			
			<table class=weapons>
					<tr class=info_title>
						<td colspan="17">穿墙数据</td>
					</tr>
					<tr class=weapon_fields>
						<td style="width:10%">武器</td>
						
						<td style="width:11.25%">穿墙击杀</td>
						<td style="width:11.25%">被穿击杀</td>
						
						<td style="width:11.25%">穿墙爆头</td>
						<td style="width:11.25%">被穿爆头</td>
						
						<td style="width:11.25%">穿墙击中</td>
						<td style="width:11.25%">被穿击中</td>
						
						<td style="width:11.25%">穿墙伤害</td>
						<td style="width:11.25%">被穿伤害</td>
					</tr>
					<?php
					$StrSubDefault = "-"; // 设置一个默认值
					for($i=0; $i<$player->m_WpSize; $i++)
					{
						if($player->m_arrWallKill[$i] == 0 && $player->m_arrBWallKill[$i] == 0 && $player->m_arrWallHead[$i] == 0 && $player->m_arrBWallHead[$i] == 0 && 
						   $player->m_arrWallHit[$i] == 0 && $player->m_arrBWallHit[$i] == 0 && $player->m_arrWallDamage[$i] == 0 && $player->m_arrBWallDamage[$i] == 0)
							continue;
						
				        echo "<tr class=data>";
						echo "<td>".$player->m_arrWImage[$i]."</td>";
						
						if($player->m_arrWallKill[$i] == 0)
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWallKill[$i]."</td>";
						if($player->m_arrBWallKill[$i] == 0)
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrBWallKill[$i]."</td>";
						
						if($player->m_arrWallHead[$i] == 0)
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWallHead[$i]."</td>";
						if($player->m_arrBWallHead[$i] == 0)
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrBWallHead[$i]."</td>";

						if($player->m_arrWallHit[$i] == 0)
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWallHit[$i]."</td>";
						if($player->m_arrBWallHit[$i] == 0)
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrBWallHit[$i]."</td>";
						
						if($player->m_arrWallDamage[$i] == 0)
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrWallDamage[$i]."</td>";
						if($player->m_arrBWallDamage[$i] == 0)
							echo "<td>".$StrSubDefault."</td>";
						else
							echo "<td>".$player->m_arrBWallDamage[$i]."</td>";
						
				        echo "</tr>";
					}
					?>
			</table>
		</div>
	</div>

	
	
	<div class=back>
		<?php 
		
		// 显示个人总榜
		$urlTemp = sprintf("<a target=\"_blank\" href=\"rank.php?uid=%d&iType=all\" style=\"color:#00ff00;\">个人总榜</a>&ensp;", $uid);
		echo "$urlTemp";
		
		// 显示个人年榜
		$urlTemp = sprintf("<a target=\"_blank\" href=\"rank.php?uid=%d&iType=year&iDate=%s\" style=\"color:#00ff00;\">%s</a>&ensp;", $uid, date("Y"), date("Y"));
		echo "$urlTemp";
		
		// 显示个人月榜
		$urlTemp = sprintf("<a target=\"_blank\" href=\"rank.php?uid=%d&iType=month&iDate=%s\" style=\"color:#00ff00;\">%s-%s</a>&ensp;", $uid, date("Ym"), date("Y"), date("m"));
		echo "$urlTemp";
		
		// 显示个人日榜
		$urlTemp = sprintf("<a target=\"_blank\" href=\"rank.php?uid=%d&iType=day&iDate=%s\" style=\"color:#00ff00;\">%s-%s-%s</a>&ensp;<br>", $uid, date("Ymd"), date("Y"), date("m"), date("d"));
		echo "$urlTemp";
		
		
		
		// 显示总榜
		$urlTemp = sprintf("<a target=\"_blank\" href=\"index.php?iType=all\" style=\"color:#00ff00;\">总排行榜</a>&ensp;");
		echo "$urlTemp";
		
		// 显示年榜
		$urlTemp = sprintf("<a target=\"_blank\" href=\"index.php?iType=year&iDate=%s\" style=\"color:#00ff00;\">%s</a>&ensp;", date("Y"), date("Y"));
		echo "$urlTemp";
		
		// 显示月榜
		$urlTemp = sprintf("<a target=\"_blank\" href=\"index.php?iType=month&iDate=%s\" style=\"color:#00ff00;\">%s-%s</a>&ensp;", date("Ym"), date("Y"), date("m"));
		echo "$urlTemp";
		
		// 显示日榜
		$urlTemp = sprintf("<a target=\"_blank\" href=\"index.php?iType=day&iDate=%s\" style=\"color:#00ff00;\">%s-%s-%s</a>&ensp;</br>", date("Ymd"), date("Y"), date("m"), date("d"));
		echo "$urlTemp";
		
		$rankUrl   = sprintf("<a target=\"_blank\" href=\"index.php?iType=%s&iDate=%s\" style=\"color:#ff0000;\">返回</a></br>", $iType, $iDate);
		echo $rankUrl;
		
		// 免费软件请保留友情链接,给个Star均表示对作者支持！
		$elapsed = microtime(true) - $start; 
		echo "<a target=\"_blank\" href=\"http://cs27015.com\" target=\"_blank\">Powered By VGStats 3.0</a>&ensp;页面生成耗时: ";
		echo $elapsed;
		echo " 秒<br><br>";
		?>
		
	</div>
</body>
</html>