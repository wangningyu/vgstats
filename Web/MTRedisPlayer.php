<?php
	require_once ("CSPub.php");
	require_once ("Military.php");

class CPlayerInfo{
	
	var	$m_Uid;			// 数字编号
	var $m_NameEn;		// 玩家英文名
	var $m_NameCh;		// 玩家中文名
	var $m_Access;		// VIP字母
	var $m_VipStr;		// VIP权限名称
	var $m_ExpDate;		// VIP到期时间
	var $m_Score;		// 玩家经验值
	var $m_RegDate;		// 注册时间
	var $m_LastLogin;	// 上次登录
	var $m_Signature;	// 玩家签名
	var $m_QQ;			// 玩家QQ
	var $m_IP;			// 上次登录IP
	var $m_Local;		// 登录位置
	
	// 以下为保留字段(范围01-15) add by MT 2023-09-13
	var $m_SteamId;		// SteamID
	var $m_VS01;		// 1号保留字符串

	var $m_nMaxPlayer;	// 最大玩家
	var $m_nRank;		// 当前排名

	var $m_Level;		// 当前等级
	var $m_Military;	// 头衔

	
	var $m_Kill;		// 杀敌
	var $m_Shot;		// 射击
	var $m_Head;		// 爆头
	var $m_Hit;			// 击中
	var $m_Damage;		// 总伤害
	var $m_Death;		// 阵亡
	var $m_FirstKill;	// 首杀
	var $m_FirstDeath;	// 首死
	var $m_Defu;		// 拆除炸弹
	var $m_Plant;		// 安装炸弹
	var $m_Online;		// 在线时长
	var $m_KillWord;	// 摔死
	var $m_KillSelf;	// 自杀
	var $m_arrHead;		// 武器爆头
	var $m_KWS;			// 自死率
	
	var $m_KD;			// KD
	var $m_HdKD;		// 爆头率
	var $m_ShotKD;		// 命中率
	var $m_arrHeadKD;	// 武器爆头率
	var $m_arrShotKD;	// 武器命中率
	
	var $m_WpSize;		// 武器个数
	var $m_arrWImage;	// 武器图标
	var $m_arrWKill;	// 武器击杀
	var $m_arrWBKill;	// 被杀次数
	var $m_arrWBHead;	// 被爆头次数
	var $m_arrWBHeadKD;	// 被爆头率
	var $m_arrWShot;	// 武器射击
	var $m_arrWHit;		// 武器击中
	var $m_arrWHead;	// 武器爆头
	var $m_arrWChest;	// 武器胸部
	var $m_arrWBelly;	// 武器腹部
	var $m_arrWArmLeft;	// 武器左臂
	var $m_arrWArmRight;// 武器右臂
	var $m_arrWLegLeft;	// 武器左腿
	var $m_arrWLegRight;// 武器右腿
	var $m_arrWShield;	// 武器盾牌
	var $m_arrWDamage;	// 武器伤害

	// 新增武器击中次数选项 add by MT 2023-09-13
	var $m_arrHitNone;		// 武器击中
	var $m_arrHitHead;		// 武器爆头
	var $m_arrHitChest;		// 武器胸部
	var $m_arrHitBelly;		// 武器腹部
	var $m_arrHitArmLeft;	// 武器左臂
	var $m_arrHitArmRight;	// 武器右臂
	var $m_arrHitLegLeft;	// 武器左腿
	var $m_arrHitLegRight;	// 武器右腿
	var $m_arrHitShield;	// 武器盾牌

	// 新增回合场次相关选项 add by MT 2023-09-30
	var $m_Round;			// 总回合
	var $m_RWinT;			// 回合：T杀完胜利
	var $m_RWinBoom;		// 回合：T爆炸胜利

	var $m_RWinCT;			// 回合：CT杀完胜利
	var $m_RWinDefuse;		// 回合：CT拆弹胜利
	var $m_RWinSaved;		// 回合：CT时间结束胜利
	var $m_RWinRescue;		// 回合：CT解救人质胜利
	var $m_RWinNotRescue;	// 回合：CT未解救人质胜利
	var $m_RWinType1;		// 回合：保留胜利1
	var $m_RWinType2;		// 回合：保留胜利2
	
	var	$m_RLose;			// 回合：失败回合
	var	$m_REven;			// 回合：平局回合
	
	var $m_Session;			// 总场次
	var $m_SWin;			// 胜利场次
	var $m_SLose;			// 失败场次
	var $m_SEven;			// 平局场次
	var $m_Mvp;				// 最佳次数
	var $m_RWS;				// 每局贡献评分
	
	var $m_Assist;			// 助攻
	var $m_RoundKill;		// 回合杀敌统计
	var $m_RoundRemain;		// 残局杀敌统计
	
	var $m_RemainRound;		// 残局回合总数
	var $m_RemainWin;		// 残局胜利次数
	var $m_RemainPeer;		// 残局胜率

	var $m_RndWinCnt;		// 回合胜利场次
	var $m_RndWinCntT;		// 回合T胜利场次
	var $m_RndWinCntCT;		// 回合CT胜利场次
	
	var $m_arrWallHit;		// 穿墙命中次数
	var $m_arrWallDamage;	// 穿墙射击伤害
	var $m_arrWallHead;		// 穿墙爆头次数
	var $m_arrWallKill;		// 穿墙击杀次数
	
	var $m_arrBWallHit;		// 被穿墙命中次数
	var $m_arrBWallDamage;	// 被穿墙射击伤害
	var $m_arrBWalHead;		// 被穿墙爆头次数
	var $m_arrBWallKill;	// 被穿墙击杀次数
	
	// 总数据
	var $m_arrAllWallHit;	// 穿墙命中次数
	var $m_arrAllWallDamage;// 穿墙射击伤害
	var $m_arrAllWallHead;	// 穿墙爆头次数
	var $m_arrAllWallKill;	// 穿墙击杀次数
	
	var $m_arrAllBWallHit;	// 被穿墙命中次数
	var $m_arrAllBWallDamage;// 被穿墙射击伤害
	var $m_arrAllBWalHead;	// 被穿墙爆头次数
	var $m_arrAllBWallKill;	// 被穿墙击杀次数
	
	//var $m_pMySQL;	// MySQL服务器
	var $m_pRedis;		// redis服务器
	var $m_iDate;		// 查询日期
	
	/*function __construct($pRedis, $sql, $vType, $iDate, $uid)
	{
		$this->m_vType = $vType;
		$this->m_Uid = $uid;
		$this->m_pRedis = $pRedis;
		$this->m_pMySQL = $sql;

		$this->get_redis_base();
		$this->get_redis_rank();
		$this->get_redis_weapon();
		$this->get_redis_userinfo();
		//$this->get_player_sql();
	}*/
	
	function __construct($pRedis, $vType, $iDate, $uid)
	{
		$this->m_vType = $vType;
		$this->m_iDate = $iDate;
		
		$this->m_Uid = $uid;
		$this->m_pRedis = $pRedis;

		$this->get_redis_base();
		$this->get_redis_rank();
		$this->get_redis_weapon();
		$this->get_redis_userinfo();
	}
	
	// 将字符串转换为网页编码
	function convert_url_str($str)
	{
		$newstr = $str;
		$newstr = str_replace("&","&amp;",$newstr);
		$newstr = str_replace("<","&lt;",$newstr);
		$newstr = str_replace(">","&gt;",$newstr);
		return $newstr;
	}
	
	function get_name_en()
	{
		return $this->m_NameEn;
	}
	function get_name_ch()
	{
		return $this->m_NameCh;
	}
	function get_vip_show()
	{
		return $this->m_VipStr;
	}
	function get_date_reg()
	{
		return $this->m_RegDate;
	}
	function get_date_login()
	{
		return $this->m_LastLogin;
	}
	function get_time_online()
	{
		return $this->m_Online;
	}
	function get_qq()
	{
		return $this->m_QQ;
	}
	function get_signature()
	{
		return $this->m_Signature;
	}
	function get_rank()
	{
		return $this->m_nRank;
	}
	function get_max_player()
	{
		return $this->m_nMaxPlayer;
	}
	function get_score()
	{
		return $this->m_Score;
	}
	function get_military()
	{
		return $this->m_Military;
	}
	function get_first_kill()
	{
		return $this->m_FirstKill;
	}
	function get_first_death()
	{
		return $this->m_FirstDeath;
	}
	function get_first_defu()
	{
		return $this->m_Defu;
	}
	function get_first_plant()
	{
		return $this->m_Plant;
	}
	function get_all_kill()
	{
		return $this->m_Kill;
	}
	function get_all_death()
	{
		return $this->m_Death;
	}
	function get_all_head()
	{
		return $this->m_Head;
	}
	function get_kd()
	{
		return $this->m_KD;
	}
	function get_head_kd()
	{
		return $this->m_HdKD;
	}
	function get_shot_kd()
	{
		return $this->m_ShotKD;
	}
	function get_all_shot()
	{
		return $this->m_Shot;
	}
	function get_all_hit()
	{
		return $this->m_Hit;
	}
	function get_kill_world()
	{
		return $this->m_KillWord;
	}
	function get_kill_self()
	{
		return $this->m_KillSelf;
	}
	function get_kws()
	{
		return $this->m_KWS;
	}
	
	function get_assist()
	{
		return $this->m_Assist;
	}
	
	function get_mvp()
	{
		$mvpPeer = "无";
		if($this->get_rnd_cnt() != 0)
			$mvpPeer = sprintf("%0.1f%%", $this->m_Mvp * 100 / $this->get_rnd_cnt());
		return $mvpPeer;
	}
	
	function get_rnd_cnt()
	{
		return $this->m_Round;
	}
	function get_rnd_win_cnt()
	{
		return $this->m_RndWinCnt;
	}
	function get_rnd_win_cntT()
	{
		return $this->m_RndWinCntT;
	}
	function get_rnd_win_cntCT()
	{
		return $this->m_RndWinCntCT;
	}
	function get_rnd_win()
	{
		return $this->m_RndWin;
	}
	function get_rnd_winT()
	{
		return $this->m_RndWinT;
	}
	function get_rnd_winCT()
	{
		return $this->m_RndWinCT;
	}
	function get_lose()
	{
		return $this->m_RLose;
	}

	// 获取玩家地理位置
	function get_location()
	{
		return $this->m_Local;
		/*$ip = $this->m_IP;
		if(strstr($ip,"192.168") || strstr($ip,"127.0"))
			return "本地局域网";
		
		$myobj = new ipLocation();
		$address = $myobj->getaddress($ip);
		$myobj = NULL;
		$address = mb_convert_encoding($address,"utf-8", "gb2312");
		if(strcmp($address,"") == 0)
			return $ip;
		elseif(strstr($address,"局域网"))
			return "本地局域网";
		else
			return $address;*/
	}
	
	// 获取权限字母
	/*function get_access_str($sAccess)
	{
		$vip1   = 'l';
		$vip2   = 'm';
		$vip3   = 'n';
		$vip4   = 'p';
		
		$flag1 = '超级管理员';
		$flag2 = '管理员';
		$flag3 = '超级VIP';
		$flag4 = 'VIP';
		$flag5 = '免费会员';
		
		if(strpos($this->m_Access, $vip1) !== false)
		{
			$this->m_VipStr = $flag1;
			return;
		}
		if(strpos($this->m_Access, $vip2) !== false)
		{
			$this->m_VipStr = $flag2;
			return;
		}
		if(strpos($this->m_Access, $vip3) !== false)
		{
			$this->m_VipStr = $flag3;
			return;
		}
		if(strpos($this->m_Access, $vip4) !== false)
		{
			$this->m_VipStr = $flag4;
			return;
		}
		$this->m_VipStr = $flag5;
		return;
	}*/
	
	// 获取当前KEY
	// a		全部
	// y2022	年份
	// y202205	月份
	/*function getKey()
	{
		$skey = "a";
		switch($this->m_vType)
		{
		case RANK_ALL:
			$skey = "a";
			break;
			
		case RANK_YEAR:
			$skey = sprintf("y%s", date("Y"));
			break;
			
		case RANK_MONTH:
			$skey = sprintf("m%s", date("Ymd"));
			break;
			
		default:
			break;
		}
		
		return $skey;
	}*/
	
	// 从SQL获取基本信息
	/*function get_player_sql()
	{
		mysql_query('set names utf8', $this->m_pMySQL);
		$ret = mysql_query("select `name`, `chsname`, `access`, `exp_date`, `xp`, `regdate`, `lastlogin`, `signature`, `qq`, `ip` from `mt_reg` where `index`=".$this->m_Uid." limit 1;", $this->m_pMySQL);
		$row = mysql_fetch_array($ret);
		$this->m_NameEn = $this->convert_url_str($row['name']);
		$this->m_NameCh = $this->convert_url_str($row['chsname']);
		$this->m_Access = $row['access'];
		$this->m_ExpDate = $row['exp_date'];
		$this->m_Score = $row['xp'];
		$this->m_RegDate = $row['regdate'];
		$this->m_LastLogin = $row['lastlogin'];
		$this->m_Signature = $row['signature'];
		$this->m_QQ = $row['qq'];
		$this->m_IP = $row['ip'];
		$this->m_VipStr = get_access_str($this->m_Access, $this->m_ExpDate);
	}*/
	
	// HGET user:1 name xxx
	var $STATS_USER		= 'user';		// 玩家账号信息
	var $USER_NAME		= 'name';		// 设置英文名
	var $USER_CHSNAME	= 'chs';		// 设置中文名
	var $USER_FLAGS		= 'flags';		// 设置玩家权限
	var $USER_REGDATE	= 'regdate';	// 设置注册时间
	var $USER_QQ		= 'qq';			// 设置QQ号
	var $USER_SIG		= 'sig';		// 设置签名
	var $USER_EXPDATE	= 'expdate';	// VIP期限
	var $USER_LASTLOGIN	= 'lastlogin';	// 设置上次登录
	var $USER_LASTIP	= 'lastip';		// 设置上次登录
	
	// 以下为保留字段 add by MT 2023-09-13
	var $USER_STEAMID	= "steamid";	// 设置steam id
	var $USER_VS01		= "vs01";		// 1号保留字符串
	var $USER_VS02		= "vs02";
	var $USER_VS03		= "vs03";
	var $USER_VS04		= "vs04";
	var $USER_VS05		= "vs05";
	var $USER_VS06		= "vs06";
	var $USER_VS07		= "vs07";
	var $USER_VS08		= "vs08";
	var $USER_VS09		= "vs09";
	var $USER_VS10		= "vs10";
	var $USER_VS11		= "vs11";
	var $USER_VS12		= "vs12";
	var $USER_VS13		= "vs13";
	var $USER_VS14		= "vs14";
	var $USER_VS15		= "vs15";

	// 加载玩家数据
	function get_redis_userinfo()
	{
		$keyPlayer = sprintf("%s:%u", $this->STATS_USER, $this->m_Uid);
		$res = $this->m_pRedis->hGetAll($keyPlayer);
		$resKeys  = array_keys($res);
		$resValues = array_values($res);
			
		//echo $keyPlayer ;
		//echo "<br>";
		
		// name
		$keyInfo = sprintf('%s', $this->USER_NAME);
		$this->m_NameEn = 'VGuard';
		if(array_key_exists($keyInfo, $res))
			$this->m_NameEn = urldecode($res[$keyInfo]);
		
		// chsname
		$keyInfo = sprintf('%s', $this->USER_CHSNAME);
		$this->m_NameCh = 'VGuard-Chs';
		if(array_key_exists($keyInfo, $res))
			$this->m_NameCh = urldecode($res[$keyInfo]);

		// regdate
		$keyInfo = sprintf('%s', $this->USER_REGDATE);
		$this->m_RegDate = '2022-06-01';
		if(array_key_exists($keyInfo, $res))
			$this->m_RegDate = urldecode($res[$keyInfo]);
		
		// qq
		$keyInfo = sprintf('%s', $this->USER_QQ);
		$this->m_QQ = 'unknow';
		if(array_key_exists($keyInfo, $res))
			$this->m_QQ = urldecode($res[$keyInfo]);
		
		// signature
		$keyInfo = sprintf('%s', $this->USER_SIG);
		$this->m_Signature = 'unknow';
		if(array_key_exists($keyInfo, $res))
			$this->m_Signature = urldecode($res[$keyInfo]);
		
		// last login
		$keyInfo = sprintf('%s', $this->USER_LASTLOGIN);
		$this->m_LastLogin = $this->m_RegDate ;
		if(array_key_exists($keyInfo, $res))
			$this->m_LastLogin = urldecode($res[$keyInfo]);
		
		// last ip
		$keyInfo = sprintf('%s', $this->USER_LASTIP);
		$this->m_IP = '127.0.0.1' ;
		if(array_key_exists($keyInfo, $res))
			$this->m_IP = urldecode($res[$keyInfo]);
		$this->m_Local = Look_IP($this->m_IP);
		
		// 以下为保留字段(范围01-15) add by MT 2023-09-13
		// steam id
		$keyInfo = sprintf('%s', $this->USER_STEAMID);
		$this->m_SteamId = $this->m_RegDate ;
		if(array_key_exists($keyInfo, $res))
			$this->m_SteamId = urldecode($res[$keyInfo]);

		// 1号保留字符串
		$keyInfo = sprintf('%s', $this->USER_VS01);
		$this->m_VS01 = 'unknow';
		if(array_key_exists($keyInfo, $res))
			$this->m_VS01 = urldecode($res[$keyInfo]);
		
		// 获取VIP类型、VIP期限
		// flags, exp_date
		$keyInfo = sprintf('%s', $this->USER_EXPDATE);
		$ExpDate = '';
		if(array_key_exists($keyInfo, $res))
			$ExpDate = urldecode($res[$keyInfo]);
		
		$keyInfo = sprintf('%s', $this->USER_FLAGS);
		$VipFlags = '';
		if(array_key_exists($keyInfo, $res))
			$VipFlags = urldecode($res[$keyInfo]);

		//echo "vipString:"; echo $VipFlags; echo "<br>";
		//echo "ExpDate:"; echo $ExpDate; echo "<br>";
		$this->m_VipStr = get_access_str($VipFlags, $ExpDate);
	}
	
	// 加载当前排名、当前积分、玩家数量
	function get_redis_rank()
	{
		$scoreKey 			= getKey($this->m_vType, $this->m_iDate);
		$this->m_nMaxPlayer = $this->m_pRedis->zcard($scoreKey);
		$this->m_Score		= $this->m_pRedis->zScore($scoreKey, $this->m_Uid);
		$this->m_nRank 		= $this->m_pRedis->zRevRank($scoreKey, $this->m_Uid) + 1;
		
		$pos = stripos($this->m_Score, '.');
		if(strlen($this->m_Score) >= ($pos + 2))
			$this->m_Score = substr($this->m_Score, 0, $pos + 2);
		//else
		//	$this->m_Score = substr($this->m_Score, 0, $pos);

		// 当前等级、头衔
		$score = $this->m_Score;
		$this->m_Level = get_user_level($score);
		$mili = get_user_military($this->m_Level);
		$this->m_Military = sprintf("(%d级) %s", $this->m_Level, $mili);
	}

	// 加载基本信息
	function get_redis_base()
	{
		$weapon = WEAPON_UNKNOW;
		$type = STATS_UNKNOW;

		$keyMain = getKey($this->m_vType, $this->m_iDate);
		$keyPlayer = sprintf("%s:%u", $keyMain, $this->m_Uid);
		$res = $this->m_pRedis->hGetAll($keyPlayer);
		$resKeys  = array_keys($res);
		$resValues = array_values($res);

		// +1kill, +2shot, +3head, +4hit, +5damge, +6death, +7fkill, +8fdeath, +9defu, +10plant, +11online
		$keyInfo = sprintf('%c%c', $weapon, STATS_KILL);
		$this->m_Kill = 0;
		if(array_key_exists($keyInfo, $res))
			$this->m_Kill = $res[$keyInfo];
			
		// +2shot
		$keyInfo = sprintf('%c%c', $weapon, STATS_SHOT);
		$this->m_Shot = 0;
		if(array_key_exists($keyInfo, $res))
			$this->m_Shot = $res[$keyInfo];

		// +3head
		$keyInfo = sprintf('%c%c', $weapon, STATS_HEADSHOT);
		$this->m_Head = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_Head = $res[$keyInfo];
		}
			
		// +4hit
		$keyInfo = sprintf('%c%c', $weapon, STATS_HIT);
		$this->m_Hit = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_Hit = $res[$keyInfo];
		}
			
		// +5damge
		$keyInfo = sprintf('%c%c', $weapon, STATS_DAMAGE);
		$this->m_Damage = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_Damage = $res[$keyInfo];
		}
			
		// +6death
		$keyInfo = sprintf('%c%c', $weapon, STATS_DEATH);
		$this->m_Death = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_Death = $res[$keyInfo];
		}
			
		// +7fkill
		$keyInfo = sprintf('%c%c', $weapon, STATS_FIRSTKILL);
		$this->m_FirstKill = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_FirstKill = $res[$keyInfo];
		}
			
		// +8fdeath
		$keyInfo = sprintf('%c%c', $weapon, STATS_FIRSTDEATH);
		$this->m_FirstDeath = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_FirstDeath = $res[$keyInfo];
		}
			
		// +9defu
		$keyInfo = sprintf('%c%c', $weapon, STATS_BOMB_DEFUSION);
		$this->m_Defu = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_Defu = $res[$keyInfo];
		}
			
		// +10plant
		$keyInfo = sprintf('%c%c', $weapon, STATS_BOMB_PLANTING);
		$this->m_Plant = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_Plant = $res[$keyInfo];
		}
			
		// +11online
		$keyInfo = sprintf('%c%c', $weapon, STATS_TIME_ONLINE);
		$onlineTime = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$onlineTime = $res[$keyInfo];
		}
		
		// +12killworld
		$keyInfo = sprintf('%c%c', $weapon, STATS_KILL_WORLD);
		$this->m_KillWord = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_KillWord = $res[$keyInfo];
		}
		
		// +13killself
		$keyInfo = sprintf('%c%c', $weapon, STATS_KILL_SELF);
		$this->m_KillSelf = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_KillSelf = $res[$keyInfo];
		}
		
		if($this->m_KillSelf != 0)
			$this->m_KWS = sprintf("%0.1f", $this->m_KillSelf * 100 / $this->m_KillWord);
		else
			$this->m_KWS = sprintf("%0.1f", $this->m_KillSelf * 100 / 100.0);
		
		$this->m_Online = sprintf("%0.1fh", $onlineTime / 3600);
			
		// kd
		if($this->m_Death != 0 )
			$this->m_KD = sprintf("%0.2f", $this->m_Kill / $this->m_Death);
		else
			$this->m_KD = sprintf("%0.1f", $this->m_Kill / 1.0);
		
		// kd head
		if($this->m_Kill != 0)
			$this->m_HdKD = sprintf("%0.1f", $this->m_Head * 100 / $this->m_Kill);
		else
			$this->m_HdKD = sprintf("%0.1f", 0.0 / 1.0);
			
		// 命中率
		if($this->m_Shot != 0 )
			$this->m_ShotKD = sprintf("%0.1f", $this->m_Hit * 100 / $this->m_Shot);
		else
			$this->m_ShotKD = sprintf("%0.1f", $this->m_Hit * 100 / 1.0);
		
		// 获取总回合
		$keyInfo = sprintf('%c%c', $weapon, STATS_ROUND);
		$this->m_Round = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_Round = $res[$keyInfo];
		}
		
		// 获取回合：T杀完胜利
		$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_T);
		$this->m_RWinT = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_RWinT = $res[$keyInfo];
		}
		
		// 获取回合：T爆炸胜利
		$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_BOOM);
		$this->m_RWinBoom = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_RWinBoom = $res[$keyInfo];
		}
		
		// 获取回合：CT杀完胜利
		$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_CT);
		$this->m_RWinCT = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_RWinCT = $res[$keyInfo];
		}
		
		// 获取回合：CT拆弹胜利
		$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_DEFUSE);
		$this->m_RWinDefuse = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_RWinDefuse = $res[$keyInfo];
		}
		
		// 获取回合：CT时间结束胜利
		$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_SAVED);
		$this->m_RWinSaved = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_RWinSaved = $res[$keyInfo];
		}
		
		// 获取回合：CT解救人质胜利
		$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_RESCUE);
		$this->m_RWinRescue = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_RWinRescue = $res[$keyInfo];
		}
		
		// 获取回合：CT未解救人质胜利
		$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_NOT_RESCUE);
		$this->m_RWinNotRescue = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_RWinNotRescue = $res[$keyInfo];
		}
		
		// 获取回合：保留胜利1
		$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_TYPE1);
		$this->m_RWinType1 = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_RWinType1 = $res[$keyInfo];
		}
		
		// 获取回合：保留胜利2
		$keyInfo = sprintf('%c%c', $weapon, STATS_RWIN_TYPE2);
		$this->m_RWinType2 = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_RWinType2 = $res[$keyInfo];
		}
		
		// 获取失败回合
		$keyInfo = sprintf('%c%c', $weapon, STATS_RLOSE);
		$this->m_RLose = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_RLose = $res[$keyInfo];
		}
		
		// 获取平局回合
		$keyInfo = sprintf('%c%c', $weapon, STATS_REVEN);
		$this->m_REven = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_REven = $res[$keyInfo];
		}
		
		// 获取比赛总场次
		$keyInfo = sprintf('%c%c', $weapon, STATS_SESSION);
		$this->m_Session = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_Session = $res[$keyInfo];
		}
		
		// 获取比赛胜利场次
		$keyInfo = sprintf('%c%c', $weapon, STATS_SWIN);
		$this->m_SWin = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_SWin = $res[$keyInfo];
		}
		
		// 获取比赛失败场次
		$keyInfo = sprintf('%c%c', $weapon, STATS_SLOSE);
		$this->m_SLose = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_SLose = $res[$keyInfo];
		}
		
		// 获取比赛平局场次
		$keyInfo = sprintf('%c%c', $weapon, STATS_SEVEN);
		$this->m_SEven = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_SEven = $res[$keyInfo];
		}
		
		// 获取最佳场次
		$keyInfo = sprintf('%c%c', $weapon, STATS_MVP);
		$this->m_Mvp = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_Mvp = $res[$keyInfo];
		}
		
		// 每局贡献评分
		$keyInfo = sprintf('%c%c', $weapon, STATS_RWS);
		$this->m_RWS = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_RWS = $res[$keyInfo];
		}
		
		
		// 助攻次数 
		$keyInfo = sprintf('%c%c', $weapon, STATS_ASSIST);
		$this->m_Assist = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_Assist = $res[$keyInfo];
		}
		
		// 每回合杀敌统计
		for($i=STATS_KILL_0; $i<=STATS_KILL_16; $i++)
		{
			$keyInfo = sprintf('%c%c', $weapon, $i);
			$this->m_RoundKill[$i - STATS_KILL_0] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_RoundKill[$i - STATS_KILL_0] = $res[$keyInfo];
			}
		}
		
		// 每回合残局统计
		for($i=STATS_1V1; $i<=STATS_1V16; $i++)
		{
			$keyInfo = sprintf('%c%c', $weapon, $i);
			$this->m_RoundRemain[$i - STATS_1V1] = 0;
			if(array_key_exists($keyInfo, $res))
			{
				$this->m_RoundRemain[$i - STATS_1V1] = $res[$keyInfo];
			}
		}
		
		// 残局回合总数
		$keyInfo = sprintf('%c%c', $weapon, STATS_1ROUND);
		$this->m_RemainRound = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_RemainRound = $res[$keyInfo];
		}
		
		// 残局胜利次数
		$keyInfo = sprintf('%c%c', $weapon, STATS_1WIN);
		$this->m_RemainWin= 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_RemainWin = $res[$keyInfo];
		}
		
		// 残局胜率
		if($this->m_RemainRound != 0)
			$this->m_RemainPeer = sprintf("%0.1f%%", $this->m_RemainWin * 100 / $this->m_RemainRound);
		else
			$this->m_RemainPeer = sprintf("%0.1f%%", $this->m_RemainWin * 100 / 1.0);
		
		// 胜利场次
		$this->m_RndWinCnt = $this->m_RWinT + $this->m_RWinBoom + $this->m_RWinCT + $this->m_RWinDefuse + $this->m_RWinSaved + $this->m_RWinRescue + $this->m_RWinNotRescue + $this->m_RWinType1 + $this->m_RWinType2;
		$this->m_RndWinCntT = $this->m_RWinT + $this->m_RWinBoom + $this->m_RWinNotRescue;
		$this->m_RndWinCntCT = $this->m_RWinCT + $this->m_RWinDefuse + $this->m_RWinSaved + $this->m_RWinRescue;

		// 总胜率
		if($this->m_Round != 0)
		{
			$this->m_RndWin = sprintf("%0.1f%%", $this->m_RndWinCnt * 100 / $this->m_Round);
		}
		else
			$this->m_RndWin = sprintf("%0.1f%%", $this->m_RndWinCnt * 100 / 1.0);
		
		// T胜利
		if(($this->m_RndWinCntT + $this->m_RndWinCntCT)  != 0)
			$this->m_RndWinT = sprintf("%0.1f%%", $this->m_RndWinCntT * 100 / ($this->m_RndWinCntT + $this->m_RndWinCntCT));
		else
			$this->m_RndWinT = sprintf("%0.1f%%", $this->m_RndWinCntT * 100 / 1.0);
		
		// CT胜利
		if(($this->m_RndWinCntT + $this->m_RndWinCntCT)  != 0)
			$this->m_RndWinCT = sprintf("%0.1f%%", $this->m_RndWinCntCT * 100 / ($this->m_RndWinCntT + $this->m_RndWinCntCT));
		else
			$this->m_RndWinCT = sprintf("%0.1f%%", $this->m_RndWinCntCT * 100 / 1.0);
		
		//////////////////////////////////////////////////////////////////////////////////////
		// 加载穿墙数据 add by MT 2023-10-05
		//////////////////////////////////////////////////////////////////////////////////////
				
		// 穿墙命中次数
		$keyInfo = sprintf('%c%c', $weapon, STATS_WALL_HIT);
		$this->m_arrAllWallHit= 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_arrAllWallHit = $res[$keyInfo];
		}
		
		// 穿墙射击伤害
		$keyInfo = sprintf('%c%c', $weapon, STATS_WALL_DAMAGE);
		$this->m_arrAllWallDamage = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_arrAllWallDamage = $res[$keyInfo];
		}
		
		// 穿墙爆头次数
		$keyInfo = sprintf('%c%c', $weapon, STATS_WALL_HEAD);
		$this->m_arrAllWallHead = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_arrAllWallHead = $res[$keyInfo];
		}
		
		// 穿墙击杀次数
		$keyInfo = sprintf('%c%c', $weapon, STATS_WALL_KILL);
		$this->m_arrAllWallKill = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_arrAllWallKill = $res[$keyInfo];
		}
		
		
		
		// 被穿墙命中次数
		$keyInfo = sprintf('%c%c', $weapon, STATS_BWALL_HIT);
		$this->m_arrAllBWallHit= 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_arrAllBWallHit = $res[$keyInfo];
		}
		
		// 被穿墙射击伤害
		$keyInfo = sprintf('%c%c', $weapon, STATS_BWALL_DAMAGE);
		$this->m_arrAllBWallDamage = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_arrAllBWallDamage = $res[$keyInfo];
		}
		
		// 被穿墙爆头次数
		$keyInfo = sprintf('%c%c', $weapon, STATS_BWALL_HEAD);
		$this->m_arrAllBWallHead = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_arrAllBWallHead = $res[$keyInfo];
		}
		
		// 被穿墙击杀次数
		$keyInfo = sprintf('%c%c', $weapon, STATS_BWALL_KILL);
		$this->m_arrAllBWallKill = 0;
		if(array_key_exists($keyInfo, $res))
		{
			$this->m_arrAllBWallKill = $res[$keyInfo];
		}
	}
	
	// 加载武器数据
	function get_redis_weapon()
	{
		$weapon = WEAPON_UNKNOW;
		$type = STATS_UNKNOW;

		$keyMain = getKey($this->m_vType, $this->m_iDate);
		$keyPlayer = sprintf("%s:%u", $keyMain, $this->m_Uid);
		$res = $this->m_pRedis->hGetAll($keyPlayer);
		$resKeys  = array_keys($res);
		$resValues = array_values($res);

		$j = 0;
		for($i=1; $i<WEAPON_MAX; $i++)
		{
			$tKill = 0;
			$tHead = 0;
			$tHit = 0;
			$tShot = 0;
			$tDamage = 0;

			$keyInfo = sprintf('%c%c', $i + $weapon, STATS_KILL);
			if(array_key_exists($keyInfo, $res))
				$tKill = $res[$keyInfo];
			
			$keyInfo = sprintf('%c%c', $i + $weapon, STATS_HEADSHOT);
			if(array_key_exists($keyInfo, $res))
				$tHead = $res[$keyInfo];
			
			$keyInfo = sprintf('%c%c', $i + $weapon, STATS_HIT);
			if(array_key_exists($keyInfo, $res))
				$tHit = $res[$keyInfo];
			
			$keyInfo = sprintf('%c%c', $i + $weapon, STATS_SHOT);
			if(array_key_exists($keyInfo, $res))
				$tShot = $res[$keyInfo];
			
			$keyInfo = sprintf('%c%c', $i + $weapon, STATS_DAMAGE);
			if(array_key_exists($keyInfo, $res))
				$tDamage = $res[$keyInfo];
			
			// 被杀次数
			$this->m_arrWBKill[$j] = '0';
			$keyInfo = sprintf('%c%c', $i + $weapon, STATS_BKILL);
			if(array_key_exists($keyInfo, $res))
				$this->m_arrWBKill[$j] = $res[$keyInfo];
				
			if($tKill || $tHead || $tHit || $tShot || $tDamage || $this->m_arrWBKill[$j])
			{
				$this->m_arrWImage[$j] 	= sprintf("<img src=\"static/%d.gif\">", $i);
				$this->m_arrWKill[$j] 	= $tKill;
				$this->m_arrHead[$j] 	= $tHead;
				$this->m_arrWHit[$j] 	= $tHit;
				$this->m_arrWShot[$j]	= $tShot;
				$this->m_arrWDamage[$j] = $tDamage;

				// 被爆头次数
				$this->m_arrWBHead[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_BHEAD);
				if(array_key_exists($keyInfo, $res))
					$this->m_arrWBHead[$j] = $res[$keyInfo];
				
				// 被爆头率 = 被爆头 / 被击杀
				$this->m_arrWBHeadKD[$j] = 0;
				if($this->m_arrWBKill[$j] != 0 )
					$this->m_arrWBHeadKD[$j] = sprintf("%0.1f%%", $this->m_arrWBHead[$j] * 100 / $this->m_arrWBKill[$j]);
				else
					$this->m_arrWBHeadKD[$j] = sprintf("%0.1f%%", $this->m_arrWBHead[$j] * 100 / 1.0);
				
				// 武器头部 = 头部伤害 / 总伤害
				$WHead = 0;
				$this->m_arrWHead[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_DMAGE_HEAD);
				if(array_key_exists($keyInfo, $res))
					$WHead = $res[$keyInfo];
				if($tDamage != 0 )
					$this->m_arrWHead[$j] = sprintf("%0.1f%%", $WHead * 100 / $tDamage);
				else
					$this->m_arrWHead[$j] = sprintf("%0.1f%%", $WHead * 100 / 1.0);
				
				//echo "Key:"; echo $keyInfo; echo "<br>";
				//echo "总伤害:"; echo $tDamage; echo "<br>";
				//echo "头伤害:"; echo $WHead; echo "<br>";
				
				// 武器胸部
				$WChest = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_DMAGE_CHEST);
				if(array_key_exists($keyInfo, $res))
					$WChest = $res[$keyInfo];
				if($tDamage != 0 )
					$this->m_arrWChest[$j] = sprintf("%0.1f%%", $WChest * 100 / $tDamage);
				else
					$this->m_arrWChest[$j] = sprintf("%0.1f%%", $WChest * 100 / 1.0);
				//echo "胸部伤害:"; echo $WChest; echo "<br>";
				
				// 武器腹部
				$WBelly = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_DMAGE_STOMACH);
				if(array_key_exists($keyInfo, $res))
					$WBelly = $res[$keyInfo];
				if($tDamage != 0 )
					$this->m_arrWBelly[$j] = sprintf("%0.1f%%", $WBelly * 100 / $tDamage);
				else
					$this->m_arrWBelly[$j] = sprintf("%0.1f%%", $WBelly * 100 / 1.0);
				//echo "胸部伤害:"; echo $WBelly; echo "<br>";
				
				// 武器左臂
				$WArmLeft = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_DMAGE_LEFTARM);
				if(array_key_exists($keyInfo, $res))
					$WArmLeft = $res[$keyInfo];
				if($tDamage != 0 )
					$this->m_arrWArmLeft[$j] = sprintf("%0.1f%%", $WArmLeft * 100 / $tDamage);
				else
					$this->m_arrWArmLeft[$j] = sprintf("%0.1f%%", $WArmLeft * 100 / 1.0);
				//echo "左臂伤害:"; echo $WArmLeft; echo "<br>";
				
				// 武器右臂
				$WArmRight = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_DMAGE_RIGHTARM);
				if(array_key_exists($keyInfo, $res))
					$WArmRight = $res[$keyInfo];
				if($tDamage != 0 )
					$this->m_arrWArmRight[$j] = sprintf("%0.1f%%", $WArmRight * 100 / $tDamage);
				else
					$this->m_arrWArmRight[$j] = sprintf("%0.1f%%", $WArmRight * 100 / 1.0);
				//echo "右臂伤害:"; echo $WArmRight; echo "<br>";
				
				// 武器左腿
				$WLegLeft = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_DMAGE_LEFTEG);
				if(array_key_exists($keyInfo, $res))
					$WLegLeft = $res[$keyInfo];
				if($tDamage != 0 )
					$this->m_arrWLegLeft[$j] = sprintf("%0.1f%%", $WLegLeft * 100 / $tDamage);
				else
					$this->m_arrWLegLeft[$j] = sprintf("%0.1f%%", $WLegLeft * 100 / 1.0);
				//echo "左腿伤害:"; echo $WLegLeft; echo "<br>";
				
				// 武器右腿
				$WLegRight = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_DMAGE_RIGHTEG);
				if(array_key_exists($keyInfo, $res))
					$WLegRight = $res[$keyInfo];
				if($tDamage != 0 )
					$this->m_arrWLegRight[$j] = sprintf("%0.1f%%", $WLegRight * 100 / $tDamage);
				else
					$this->m_arrWLegRight[$j] = sprintf("%0.1f%%", $WLegRight * 100 / 1.0);
				//echo "右腿伤害:"; echo $WLegRight; echo "<br>";
				
				// 武器盾牌
				$WShield = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_DMAGE_SHIELD);
				if(array_key_exists($keyInfo, $res))
					$WShield = $res[$keyInfo];
				if($tDamage != 0 )
					$this->m_arrWShield[$j] = sprintf("%0.1f%%", $WShield * 100 / $tDamage);
				else
					$this->m_arrWShield[$j] = sprintf("%0.1f%%", $WShield * 100 / 1.0);
				//echo "盾牌伤害:"; echo $WShield; echo "<br>";

				// 爆头率
				if($this->m_arrWKill[$j] != 0 )
					$this->m_arrHeadKD[$j] = sprintf("%0.1f%%", $this->m_arrHead[$j] * 100 / $this->m_arrWKill[$j]);
				else
					$this->m_arrHeadKD[$j] = sprintf("%0.1f%%", $this->m_arrHead[$j] * 100 / 1.0);
				
				// 命中率
				if($this->m_arrWShot[$j] != 0 )
					$this->m_arrShotKD[$j] = sprintf("%0.1f%%", $this->m_arrWHit[$j] * 100 / $this->m_arrWShot[$j]);
				else
					$this->m_arrShotKD[$j] = sprintf("%0.1f%%", $this->m_arrWHit[$j] * 100 / 1.0);
				
				//////////////////////////////////////////////////////////////////////////////////////
				// 加载武器击次数数据 add by MT 2023-09-13
				//////////////////////////////////////////////////////////////////////////////////////
				
				// 前武器命中次数
				$WeapHit = 1;
				if($this->m_arrWHit[$j] != 0)
					$WeapHit = $this->m_arrWHit[$j];
				
				// 击中头部次数
				$HitHead = 0;
				$this->m_arrHitHead[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_HIT_HEAD);
				if(array_key_exists($keyInfo, $res))
					$HitHead = $res[$keyInfo];
				if($HitHead)
					$this->m_arrHitHead[$j] = sprintf("%0.1f%%", $HitHead * 100 / $WeapHit);	//$HitHead;
				else
					$this->m_arrHitHead[$j] = '-';
				
				// 击中胸部次数
				$HitChest = 0;
				$this->m_arrHitChest[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_HIT_CHEST);
				if(array_key_exists($keyInfo, $res))
					$HitChest = $res[$keyInfo];
				if($HitChest)
					$this->m_arrHitChest[$j] = sprintf("%0.1f%%", $HitChest * 100 / $WeapHit);	//$HitChest;
				else
					$this->m_arrHitChest[$j] = '-';
				
				// 击中胃部次数
				$HitBelly = 0;
				$this->m_arrHitBelly[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_HIT_STOMACH);
				if(array_key_exists($keyInfo, $res))
					$HitBelly = $res[$keyInfo];
				if($HitBelly)
					$this->m_arrHitBelly[$j] = sprintf("%0.1f%%", $HitBelly * 100 / $WeapHit);	//$HitBelly;
				else
					$this->m_arrHitBelly[$j] = '-';
				
				// 击中左臂次数
				$HitLeftArm = 0;
				$this->m_arrHitArmLeft[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_HIT_LEFTARM);
				if(array_key_exists($keyInfo, $res))
					$HitLeftArm = $res[$keyInfo];
				if($HitLeftArm)
					$this->m_arrHitArmLeft[$j] = sprintf("%0.1f%%", $HitLeftArm * 100 / $WeapHit);	//$HitLeftArm;
				else
					$this->m_arrHitArmLeft[$j] = '-';
				
				// 击中右臂次数
				$HitRightArm = 0;
				$this->m_arrHitArmRight[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_HIT_RIGHTARM);
				if(array_key_exists($keyInfo, $res))
					$HitRightArm = $res[$keyInfo];
				if($HitRightArm)
					$this->m_arrHitArmRight[$j] = sprintf("%0.1f%%", $HitRightArm * 100 / $WeapHit);//$HitRightArm;
				else
					$this->m_arrHitArmRight[$j] = '-';
				
				// 击中左脚次数
				$HitLeftLeg = 0;
				$this->m_arrHitLegLeft[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_HIT_LEFTEG);
				if(array_key_exists($keyInfo, $res))
					$HitLeftLeg = $res[$keyInfo];
				if($HitLeftLeg)
					$this->m_arrHitLegLeft[$j] = sprintf("%0.1f%%", $HitLeftLeg * 100 / $WeapHit);	//$HitLeftLeg;
				else
					$this->m_arrHitLegLeft[$j] = '-';
				
				// 击中右脚次数
				$HitRightLeg = 0;
				$this->m_arrHitLegRight[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_HIT_RIGHTEG);
				if(array_key_exists($keyInfo, $res))
					$HitRightLeg = $res[$keyInfo];
				if($HitRightLeg)
					$this->m_arrHitLegRight[$j] = sprintf("%0.1f%%", $HitRightLeg * 100 / $WeapHit);//$HitRightLeg;
				else
					$this->m_arrHitLegRight[$j] = '-';
				
				// 击中盾牌次数
				$HitShield = 0;
				$this->m_arrHitShield[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_HIT_SHIELD);
				if(array_key_exists($keyInfo, $res))
					$HitShield = $res[$keyInfo];
				if($HitShield)
					$this->m_arrHitShield[$j] = sprintf("%0.1f%%", $HitShield * 100 / $WeapHit);	//$HitShield;
				else
					$this->m_arrHitShield[$j] = '-';
				
				//////////////////////////////////////////////////////////////////////////////////////
				// 穿墙命中次数
				$this->m_arrWallHit[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_WALL_HIT);
				if(array_key_exists($keyInfo, $res))
					$this->m_arrWallHit[$j] = $res[$keyInfo];
				
				// 穿墙射击伤害
				$this->m_arrWallDamage[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_WALL_DAMAGE);
				if(array_key_exists($keyInfo, $res))
					$this->m_arrWallDamage[$j] = $res[$keyInfo];
				
				// 穿墙爆头次数
				$this->m_arrWallHead[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_WALL_HEAD);
				if(array_key_exists($keyInfo, $res))
					$this->m_arrWallHead[$j] = $res[$keyInfo];
				
				// 穿墙击杀次数
				$this->m_arrWallKill[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_WALL_KILL);
				if(array_key_exists($keyInfo, $res))
					$this->m_arrWallKill[$j] = $res[$keyInfo];
				
				//////////////////////////////////////////////////////////////////////////////////////
				// 被穿墙命中次数
				$this->m_arrBWallHit[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_BWALL_HIT);
				if(array_key_exists($keyInfo, $res))
					$this->m_arrBWallHit[$j] = $res[$keyInfo];
				
				// 被穿墙射击伤害
				$this->m_arrBWallDamage[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_BWALL_DAMAGE);
				if(array_key_exists($keyInfo, $res))
					$this->m_arrBWallDamage[$j] = $res[$keyInfo];
				
				// 被穿墙爆头次数
				$this->m_arrBWallHead[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_BWALL_HEAD);
				if(array_key_exists($keyInfo, $res))
					$this->m_arrBWallHead[$j] = $res[$keyInfo];
				
				// 被穿墙击杀次数
				$this->m_arrBWallKill[$j] = 0;
				$keyInfo = sprintf('%c%c', $i + $weapon, STATS_BWALL_KILL);
				if(array_key_exists($keyInfo, $res))
					$this->m_arrBWallKill[$j] = $res[$keyInfo];
				
				$j++;
			}
		}
		
		$this->m_WpSize = $j;
		
		//echo "this->m_WpSize:";
		//echo $this->m_WpSize;
		//echo "<br>";
	}
	
	// end
}

?>
